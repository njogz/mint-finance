<?php
include "../jobs/jobs.php";
include "../users/users.php";
include "finance.php";

$jobs = new jobs;
$finance = new finance;
$user = new users;


$option=$_POST['option'];
$cash=$_POST['cash'];
$details=$_POST['details'];
$id=$_GET['id'];
$userid=$_COOKIE['id'];
$date=date("Y-m-d");
#check that amount entered is not more than total
$select="SELECT * FROM job_card WHERE card_id=$id";
$selectall=mysql_query($select);
$job=mysql_fetch_object($selectall);
$client_id=$job->client_id;
$client_name=$jobs->get_client_details('first_name',$client_id);
if ($id==0){
	$type="Misc income";
}
else{
	$type=$client_name;
}
if ($id==0){
$amountpaid=0;
$amountpayable=1;
}
else{
$amountpaid=$job->amount_paid+$cash;
$amountpayable=1.16*($job->total);
$amountpaid=(int)$amountpaid;
$amountpayable=(int)$amountpayable;
}
if ($amountpaid>$amountpayable){
//echo "<p align='center' class='style1'>The amount should not be more than Ksh".$amountpayable." \n Please close this window and enter again</p>";
echo "<META HTTP-EQUIV=\"Refresh\" CONTENT=\"1; URL=payment.php?id=0&m=more&amount=$amountpayable\">";
}

else{
if ($option=="ADD TO CASHBOX"){
	#update the cashbox
	$total=$finance->check_cashbox_balance()+$cash;
	if($finance->update_cashbox($total, $cash, $type, 'addition', $details)){
		#keep record of payment
		$transaction_id="cb".$finance->check_cashbox_id();
		if ($id==0){
		if($finance->enter_payment1($cash,$id,$transaction_id,$details)){
					$balance=$finance->check_cashbox_balance();
					echo "<META HTTP-EQUIV=\"Refresh\" CONTENT=\"1; URL=payment.php?id=0&m=success&balance=$balance\">";
			}
		}
		else{
			if($finance->enter_payment($cash,$id,$transaction_id)){
				#update job card payment records
				if($jobs->update_card_paid($id,$cash)){
					$balance=$finance->check_cashbox_balance();
					echo "<META HTTP-EQUIV=\"Refresh\" CONTENT=\"1; URL=../clients/client.php?id=$client_id&m=success&balance=$balance\">";
				}
			}
		}
	}
}
else if ($option=="Bank of Baroda account"){
	#update bank account
	$total=$finance->check_bank_baroda_balance()+$cash;
	if($finance->update_bank_baroda($total, $cash, $type)){
		#keep record of payment
		$transaction_id="bb".$finance->check_bank_baroda_id();
		if ($id==0){
		if($finance->enter_payment1($cash,$id,$transaction_id,$details)){
				$balance=$finance->check_bank_baroda_balance();
				echo "<META HTTP-EQUIV=\"Refresh\" CONTENT=\"1; URL=payment.php?id=0&m=success&balance=$balance\">";
			}
		}
		else{
			if($finance->enter_payment($cash,$id,$transaction_id)){
				#update job card payment records
				if($jobs->update_card_paid($id,$cash)){
					$balance=$finance->check_bank_baroda_balance();
					echo "<META HTTP-EQUIV=\"Refresh\" CONTENT=\"1; URL=../clients/client.php?id=$client_id&m=success&balance=$balance\">";
				}
			}
		}
	}
}
else if ($option=="HFCK Bank account"){
	#update bank account
	$total=$finance->check_bank_hfck_balance()+$cash;
	if($finance->update_bank_hfck($total, $cash, $type)){
		#keep record of payment
		$transaction_id="bh".$finance->check_bank_hfck_id();
		if ($id==0){
		if($finance->enter_payment1($cash,$id,$transaction_id,$details)){
				$balance=$finance->check_bank_hfck_balance();
				echo "<META HTTP-EQUIV=\"Refresh\" CONTENT=\"1; URL=payment.php?id=0&m=success&balance=$balance\">";
			}
		}
		else{
			if($finance->enter_payment($cash,$id,$transaction_id)){
				#update job card payment records
				if($jobs->update_card_paid($id,$cash)){
					$balance=$finance->check_bank_hfck_balance();
					echo "<META HTTP-EQUIV=\"Refresh\" CONTENT=\"1; URL=../clients/client.php?id=$client_id&m=success&balance=$balance\">";
				}
			}
		}
	}
}
else if ($option=="Rafiki DTM account"){
	#update bank account
	$total=$finance->check_bank_rafiki_balance()+$cash;
	if($finance->update_bank_rafiki($total, $cash, $type)){
		#keep record of payment
		$transaction_id="br".$finance->check_bank_rafiki_id();
		if ($id==0){
		if($finance->enter_payment1($cash,$id,$transaction_id,$details)){
				$balance=$finance->check_bank_rafiki_balance();
				echo "<META HTTP-EQUIV=\"Refresh\" CONTENT=\"1; URL=payment.php?id=0&m=success&balance=$balance\">";
			}
		}
		else{
			if($finance->enter_payment($cash,$id,$transaction_id)){
				#update job card payment records
				if($jobs->update_card_paid($id,$cash)){
					$balance=$finance->check_bank_rafiki_balance();
					echo "<META HTTP-EQUIV=\"Refresh\" CONTENT=\"1; URL=../clients/client.php?id=$client_id&m=success&balance=$balance\">";
				}
			}
		}
	}
}
else{
echo "<p align='center' class='style1'>Something went wrong \n Please close this window and try again</p>";
}
}
?>
