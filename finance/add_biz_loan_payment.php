<?php
#set all the varibles
include "../users/users.php";
include "finance.php";
include "../finance_config.php";

$finance = new finance;
$users = new users;
$account=$_POST['account'];
$amount=$_POST['amount'];
$loanid=$_POST['loanid'];
$date=date("d-m-Y");
$userid=$_COOKIE['id'];
$type="Loan Payment";

#if account not selected
if ($account=="Select one"){
	echo "<META HTTP-EQUIV=\"Refresh\" CONTENT=\"1;	URL=pay_biz_loan.php?id=$loanid&m=error\">";	
}

#if a cashbox transaction
if ($account=="Cashbox"){
	#update the cashbox
	$total=$finance->check_cashbox_balance()-$amount;
	if($finance->update_cashbox($total, $amount, $type, 'addition')){
		#keep record of payment
		$transaction_id="cb".$finance->check_cashbox_id();
		if ($finance->pay_biz_loan($amount,$loanid)){
			$balance=$finance->check_cashbox_balance();
			echo "<META HTTP-EQUIV=\"Refresh\" CONTENT=\"1;	URL=biz_loans.php?m=success&balance=$balance\">";
		}
	}	
}
else if($account=="Bank of Baroda account"){
	#update the account
	$total=$finance->check_bank_baroda_balance()-$amount;
	if($finance->update_bank_baroda($total, $amount, $type)){
		#keep record of payment
		$transaction_id="cb".$finance->check_bank_baroda_id();
		if ($users->pay_biz_loan($amount,$loanid)){
			$balance=$finance->check_bank_baroda_balance();
			echo "<META HTTP-EQUIV=\"Refresh\" CONTENT=\"1;	URL=biz_loans.php?m=success&balance=$balance\">";
		}
	}
}
else if($account=="HFCK Bank account"){
	#update the account
	$total=$finance->check_bank_hfck_balance()-$amount;
	if($finance->update_bank_hfck($total, $amount, $type)){
		#keep record of payment
		$transaction_id="cb".$finance->check_bank_hfck_id();
		if ($users->pay_biz_loan($amount,$loanid)){
			$balance=$finance->check_bank_hfck_balance();
			echo "<META HTTP-EQUIV=\"Refresh\" CONTENT=\"1;	URL=biz_loans.php?m=success&balance=$balance\">";
		}
	}
}

else if($account=="Rafiki DTM account"){
	#update the account
	$total=$finance->check_bank_rafiki_balance()-$amount;
	if($finance->update_bank_rafiki($total, $amount, $type)){
		#keep record of payment
		$transaction_id="cb".$finance->check_bank_rafiki_id();
		if ($users->pay_biz_loan($amount,$loanid)){
			$balance=$finance->check_bank_rafiki_balance();
			echo "<META HTTP-EQUIV=\"Refresh\" CONTENT=\"1;	URL=biz_loans.php?m=success&balance=$balance\">";
		}
	}
}
?>
