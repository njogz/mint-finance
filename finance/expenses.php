<?php
if (!isset($_COOKIE['id'])) {
	header("location:index.php");
}
include "../users/users.php";
include "../jobs/jobs.php";
include "finance.php";
include "../finance_config.php";

$user = new users;
$finance = new finance;
$job = new jobs;
$client=new clients;
?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <title>Mintgraphics Finance</title>
    <meta content="IE=edge,chrome=1" http-equiv="X-UA-Compatible">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="Njuguna Ndung'u">

    <link rel="stylesheet" type="text/css" href="../lib/bootstrap/css/bootstrap.css">
    
    <link rel="stylesheet" type="text/css" href="../stylesheets/theme.css">
    <link rel="stylesheet" href="../lib/font-awesome/css/font-awesome.css">

    <script src="../lib/jquery-1.7.2.min.js" type="text/javascript"></script>

    <!-- Demo page code -->

    <style type="text/css">
        #line-chart {
            height:300px;
            width:800px;
            margin: 0px auto;
            margin-top: 1em;
        }
        .brand { font-family: georgia, serif; }
        .brand .first {
            color: #ccc;
            font-style: italic;
        }
        .brand .second {
            color: #fff;
            font-weight: bold;
        }
    </style>

    <!-- Le HTML5 shim, for IE6-8 support of HTML5 elements -->
    <!--[if lt IE 9]>
      <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->

    <!-- Le fav and touch icons -->
    <link rel="shortcut icon" href="../assets/ico/favicon.ico">
    <link rel="apple-touch-icon-precomposed" sizes="144x144" href="../../assets/ico/apple-touch-icon-144-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="114x114" href="../../assets/ico/apple-touch-icon-114-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="72x72" href="../../assets/ico/apple-touch-icon-72-precomposed.png">
    <link rel="apple-touch-icon-precomposed" href="../../assets/ico/apple-touch-icon-57-precomposed.png">
  </head>

  <!--[if lt IE 7 ]> <body class="ie ie6"> <![endif]-->
  <!--[if IE 7 ]> <body class="ie ie7 "> <![endif]-->
  <!--[if IE 8 ]> <body class="ie ie8 "> <![endif]-->
  <!--[if IE 9 ]> <body class="ie ie9 "> <![endif]-->
  <!--[if (gt IE 9)|!(IE)]><!--> 
  <body class=""> 
  <!--<![endif]-->
    
    <div class="navbar">
        <div class="navbar-inner">
                <ul class="nav pull-right">
                    
                    <!-- <li><a href="#" class="hidden-phone visible-tablet visible-desktop" role="button">Settings</a></li> -->
                    <li id="fat-menu" class="dropdown">
                        <a href="#" role="button" class="dropdown-toggle" data-toggle="dropdown">
                            <i class="icon-user"></i>
                            <?php
								$name = $user->get_user("user_name",$_COOKIE['id']);
								echo $name;                            
                            ?>
                            <i class="icon-caret-down"></i>
                        </a>

                        <ul class="dropdown-menu">
							<?php
								define('ROOT', getcwd());
								require_once ROOT . '../../menu/usermenu.php';
							?>
                        </ul>
                    </li>
                    
                </ul>
                <a class="brand" href="index.html"><span class="first">Mint</span> <span class="second">Graphics</span></a>
        </div>
    </div>
    <div class="sidebar-nav">
        <form class="search form-inline">
            <input type="text" placeholder="Search...">
        </form>

        <?php
			define('ROOT', getcwd());
			require_once ROOT . '../../menu/sidemenu.php';
		?>
    </div>
    

    
    <div class="content">
        
        <div class="header">
            
            <h1 class="page-title">
				<?php if($_GET[id]==1){
					echo "Add admin expense";
					}
					else{
						echo "Add job expnse";
					}
				?>
            </h1>
        </div>
        
                <ul class="breadcrumb">
            <li><a href="../dashboard.php">Home</a> <span class="divider">/</span></li>
            <li class="active">Finance <span class="divider">/</span></li>
            
				<?php if($_GET[id]==1){
					echo "<li class='active'>Add admin expense</li>";
					}
					else{
						$clientid=$job->get_card_field('client_id',$_GET[id]);
						$client_name=$client->get_client('first_name',$clientid);
						echo "<li><a href='../clients/client.php?id=".$clientid."'>".$client_name."</a><span class='divider'>/</span></li>";
						echo "<li class='active'>Add job expnse</li>";
					}
				?>
            </li>
        </ul>

        <div class="container-fluid">
            <div class="row-fluid">
                    

<div class="row-fluid">
	<?php
	if(isset($_GET['m'])){
		if($_GET['m']=='not_enough'){
			$account=$_GET['account'];
			echo '<div class="alert alert-error">
			<button type="button" class="close" data-dismiss="alert">×</button>
			The '.$account.' account does not have enough money to handle this transaction
			</div>';
		}
		elseif($_GET['m']=='success'){
			$type=$_GET['type'];
			$balance=$_GET['balance'];
			echo '<div class="alert alert-success">
			<button type="button" class="close" data-dismiss="alert">×</button>
			'.$type.' expense successfuly added. <strong>New balance is:'.$balance.'</strong>
			</div>';
		}
	}
	?>
    <div class="block ">
        	<?php
			$id=$_GET['id'];
			?>
			<form id="form1" name="form1" method="post" action="<?php echo "enter_expense.php?id=$id"; ?>">
			  <p>
				<label>Amount</label>
				<input type="text" name="cash" id="cash" required />
			  </p>
			  <label>Type</label>
			  <select name="type" id="type">
				  <?php
					if($id!=1){
				  ?>
					<option>Binding</option>
					<option>Broad-base stand</option>
					  <option>Buttons</option>
					  <option>CD/DVD</option>
					  <option>Commission Fees on Transactions</option>
					  <option>Commission on Sales</option>
					  <option>Creasing</option>
					  <option>Cutting</option>
					  <option>Cutting-Dye</option>
					  <option>Cutting-Vinyl</option>
					  <option>Diaries</option>
					  <option>Digital Printing</option>
					  <option>Discount on Sales</option>
					  <option>Entertainment</option>
					  <option>Envelope</option>
					  <option>Facilitation fee</option>
					  <option>fabric Printing</option>
					  <option>Films</option>
					  <option>Gifts and donations</option>
					  <option>Ink & Toner</option>
					  <option>Lamination</option>
					  <option>Lanyard</option>
					  <option>Large Format Printing</option>
					  <option>Laser Engraving</option>
					  <option>Miscellaneous</option>
					  <option>Offset Printing</option>
					  <option>Packaging</option>
					  <option>Paper</option>
					  <option>Perforation</option>
					  <option>Picture frame</option>
					  <option>Plates</option>
					  <option>Professional fees</option>
					  <option>PVC</option>
					  <option>Ribbons</option>
					  <option>Screen printing</option>
					  <option>Snapper frame</option>
					  <option>Transport</option>
					  <option>T-shirts</option>
					  <option>Umbrellas</option>
					  <option>Vinyl</option>
				  <?php
					}
					else{
				  ?>
						<option>Airtime</option>
						<option>Advertising</option>
						<option>Commission Fees on Transactions</option>
						<option>Computer Accessories</option>
						<option>Cleaning</option>
						<option>Décor</option>
						<option>Entertainment</option>
						<option>Furniture & Fittings</option>
						<option>licences and fees</option>
						<option>Miscellaneous</option>
						<option>Professional fees</option>
						<option>Reimbursements</option>
						<option>Rent</option>
						<option>Salary</option>
						<option>Sanitary</option>
						<option>Software</option>
						<option>Stationery</option>
						<option>Tax</option>
						<option>Transport</option>
				  <?php
					}
				  ?>
			  </select>
			  <p>
				<label>Details</label>
				<input type="text" name="details" id="details" />
				<label>OPTION</label>
				<select name="option" id="option">
					<option>Select one</option>
					<option>REMOVE FROM CASHBOX</option>
					<option>Bank of Baroda account</option>
					<option>HFCK Bank account</option>
					<option>Rafiki DTM account</option>
					<option>Personal Money</option>
				</select>
			  </p>
			  <p>
				<label>
				<input class="btn btn-primary" type="submit" name="add" id="add" value="Submit" />
				</label>
			  </p>
</form>
    </div>
</div>

                    
                    <footer>
                        <hr>
                        
                        <p>&copy; <?php echo date('Y')?> Mint Graphics</p>
                    </footer>
                    
            </div>
        </div>
    </div>
    


    <script src="../lib/bootstrap/js/bootstrap.js"></script>
    <script type="text/javascript">
        $("[rel=tooltip]").tooltip();
        $(function() {
            $('.demo-cancel-click').click(function(){return false;});
        });
    </script>
    
  </body>
</html>


