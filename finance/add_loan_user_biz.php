<?php
#set all the varibles from the form
include "../users/users.php";
include "finance.php";
include "../finance_config.php";

$finance = new finance;
$users = new users;
$account=$_POST['account'];
$amount=$_POST['amount'];
$date=date("d-m-Y");
$userid=$_POST['from'];
$name=$users->get_user("user_name",$userid);
#if a cashbox transaction
if ($account=="Cashbox"){
	#update cashbox
	$total=$finance->check_cashbox_balance()+$amount;
	$type="Loan from ".$users->get_user("user_name",$userid);
	if ($finance->update_cashbox($total, $amount, $type, 'addition')){
		$balance=$finance->check_cashbox_balance();
		if ($users->update_money_owed($amount, $userid)){
			$owed=$users->get_user("money_owed",$userid);
			echo "<META HTTP-EQUIV=\"Refresh\" CONTENT=\"1; URL=loan_user_biz.php?m=success&balance=$balance&user=$name&owed=$owed\">";
		}
	}
}
else if($account=="Bank of Baroda account"){
	#update account
	$total=$finance->check_bank_baroda_balance()+$amount;
	$type="Loan from ".$users->get_user("user_name",$userid);
	if ($finance->update_bank_baroda($total, $amount, $type)){
		$balance=$finance->check_bank_baroda_balance();
		if ($users->update_money_owed($amount, $userid)){
			$owed=$users->get_user("money_owed",$userid);
			echo "<META HTTP-EQUIV=\"Refresh\" CONTENT=\"1; URL=loan_user_biz.php?m=success&balance=$balance&user=$name&owed=$owed\">";
		}
	}
}
else if($account=="HFCK Bank account"){
	#update account
	$total=$finance->check_bank_hfck_balance()+$amount;
	$type="Addition";
	if ($finance->update_bank_hfck($total, $amount, $type)){
		$balance=$finance->check_bank_hfck_balance();
		if ($users->update_money_owed($amount, $userid)){
			$owed=$users->get_user("money_owed",$userid);
			echo "<META HTTP-EQUIV=\"Refresh\" CONTENT=\"1; URL=loan_user_biz.php?m=success&balance=$balance&user=$name&owed=$owed\">";
		}
	}
}
else if($option=="Rafiki DTM account"){
	#update account
	$total=$finance->check_bank_rafiki_balance()+$amount;
	$type="Addition";
	if ($finance->update_bank_rafiki($total, $amount, $type)){
		$balance=$finance->check_bank_rafiki_balance();
		if ($users->update_money_owed($amount, $userid)){
			$owed=$users->get_user("money_owed",$userid);
			echo "<META HTTP-EQUIV=\"Refresh\" CONTENT=\"1; URL=loan_user_biz.php?m=success&balance=$balance&user=$name&owed=$owed\">";
		}
	}
}

?>
