<?php
#set all the varibles from the form
include "../users/users.php";
include "finance.php";
include "../finance_config.php";

$finance = new finance;
$users = new users;

$option=$_POST['option'];
$amount=$_POST['cash'];
$date=date("d-m-Y");
$type=$_POST['type'];
$id=$_GET['id'];
$userid=$_COOKIE['id'];
$details=$_POST['details'];
#if a cashbox transaction
if ($option=="REMOVE FROM CASHBOX"){
	#Check if balance is enough
	if ($amount>$finance->check_cashbox_balance()){
		echo "<META HTTP-EQUIV=\"Refresh\" CONTENT=\"1; URL=expenses.php?id=$id&m=not_enough&account=cashbox\">";
	}
	else{
		#update cashbox
		$total=$finance->check_cashbox_balance()-$amount;
		if ($finance->update_cashbox($total, $amount, $type, 'deduction', $details)){
			$balance=$finance->check_cashbox_balance();
			$transaction_id="cb".$finance->check_cashbox_id();
			#set the expense as administrative or operational
			if ($id==1){
				if ($finance->enter_admin_expense($amount, $type, $id, $transaction_id, $details)){
					echo "<META HTTP-EQUIV=\"Refresh\" CONTENT=\"1; URL=expenses.php?id=$id&m=success&type=Admin&balance=$balance\">";
				}
			}
			else{
				if ($finance->enter_operation_expense($amount, $type, $id, $transaction_id, $details)){
					echo "<META HTTP-EQUIV=\"Refresh\" CONTENT=\"1; URL=expenses.php?id=$id&m=success&type=Job&balance=$balance\">";
				}
			}
		}
	}
}
else if($option=="Bank of Baroda account"){
	#Check if balance is enough
	if ($amount>$finance->check_bank_baroda_balance()){
		echo "<META HTTP-EQUIV=\"Refresh\" CONTENT=\"1; URL=expenses.php?id=$id&m=not_enough&account=baroda\">";
	}
	else{
		#update account
		$total=$finance->check_bank_baroda_balance()-$amount;
		//$type="Deduction";
		if ($finance->update_bank_baroda($total, $amount, $type)){
			$balance=$finance->check_bank_baroda_balance();
			$transaction_id="bb".$finance->check_bank_baroda_id();
			#set the expense as administrative or operational
			if ($id==1){
				if ($finance->enter_admin_expense($amount, $type, $id, $transaction_id, $details)){
					echo "<META HTTP-EQUIV=\"Refresh\" CONTENT=\"1; URL=expenses.php?id=$id&m=success&type=Admin&balance=$balance\">";
				}
			}
			else{
				if ($finance->enter_operation_expense($amount, $type, $id, $transaction_id, $details)){
					echo "<META HTTP-EQUIV=\"Refresh\" CONTENT=\"1; URL=expenses.php?id=$id&m=success&type=Job&balance=$balance\">";
				}
			}
		}
	}
}
else if($option=="HFCK Bank account"){
	#Check if balance is enough
	if ($amount>$finance->check_bank_hfck_balance()){
		echo "<META HTTP-EQUIV=\"Refresh\" CONTENT=\"1; URL=expenses.php?id=$id&m=not_enough&account=hfck\">";
	}
	else{
		#update account
		$total=$finance->check_bank_hfck_balance()-$amount;
		//$type="Deduction";
		if ($finance->update_bank_hfck($total, $amount, $type)){
			$balance=$finance->check_bank_hfck_balance();
			$transaction_id="bh".$finance->check_bank_hfck_id();
			#set the expense as administrative or operational
			if ($id==1){
				if ($finance->enter_admin_expense($amount, $type, $id, $transaction_id, $details)){
					echo "<META HTTP-EQUIV=\"Refresh\" CONTENT=\"1; URL=expenses.php?id=$id&m=success&type=Admin&balance=$balance\">";
				}
			}
			else{
				if ($finance->enter_operation_expense($amount, $type, $id, $transaction_id, $details)){
					echo "<META HTTP-EQUIV=\"Refresh\" CONTENT=\"1; URL=expenses.php?id=$id&m=success&type=Job&balance=$balance\">";
				}
			}
		}
	}
}
else if($option=="Rafiki DTM account"){
	#Check if balance is enough
	if ($amount>$finance->check_bank_rafiki_balance()){
		echo "<META HTTP-EQUIV=\"Refresh\" CONTENT=\"1; URL=expenses.php?id=$id&m=not_enough&account=rafiki\">";
	}
	else{
		#update account
		$total=$finance->check_bank_rafiki_balance()-$amount;
		//$type="Deduction";
		if ($finance->update_bank_rafiki($total, $amount, $type)){
			$balance=$finance->check_bank_rafiki_balance();
			$transaction_id="br".$finance->check_bank_rafiki_id();
			#set the expense as administrative or operational
			if ($id==1){
				if ($finance->enter_admin_expense($amount, $type, $id, $transaction_id, $details)){
					echo "<META HTTP-EQUIV=\"Refresh\" CONTENT=\"1; URL=expenses.php?id=$id&m=success&type=Admin&balance=$balance\">";
				}
			}
			else{
				if ($finance->enter_operation_expense($amount, $type, $id, $transaction_id, $details)){
					echo "<META HTTP-EQUIV=\"Refresh\" CONTENT=\"1; URL=expenses.php?id=$id&m=success&type=Job&balance=$balance\">";
				}
			}
		}
	}
}
else if($option=="Personal Money"){
	#update money owed to user
	if ($users->update_money_owed($amount, $userid)){
		$balance=$users->get_user("money_owed",$userid);
		$transaction_id="user".$userid;
		#set the expense as administrative or operational
			if ($id==1){
				if ($finance->enter_admin_expense($amount, $type, $id, $transaction_id, $details)){
					echo "<META HTTP-EQUIV=\"Refresh\" CONTENT=\"1; URL=expenses.php?id=$id&m=success&type=Admin&balance=$balance\">";
				}
			}
			else{
				if ($finance->enter_operation_expense($amount, $type, $id, $transaction_id, $details)){
					echo "<META HTTP-EQUIV=\"Refresh\" CONTENT=\"1; URL=expenses.php?id=$id&m=success&type=Job&balance=$balance\">";
				}
			}
	}	
}
else if($option=="Payables"){
	if ($finance->update_payables($amount, $details)){
		echo "Expense added to payables ";
		$transaction_id="payable".$userid;
		#set the expense as administrative or operational
			if ($id==1){
				if ($finance->enter_admin_expense($amount, $type, $id, $transaction_id, $details)){
					echo "<p align='center' class='style1'>Admin expense entered. We good</p>";
				}
			}
			else{
				if ($finance->enter_operation_expense($amount, $type, $id, $transaction_id, $details)){
					echo "<p align='center' class='style1'>Job expense entered. We good</p>";
				}
			}
	}
	else{
		echo "Something went wrong";
	}	
}
?>
