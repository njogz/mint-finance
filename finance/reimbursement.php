<?php
#set all the varibles
include "../users/users.php";
include "finance.php";
include "../finance_config.php";

$finance = new finance;
$users = new users;
$amount=$_POST['amount'];
$date=date("Y-m-d");
$name=$_POST['user'];
$details=$_POST['details'];
$option=$_POST['account'];

#if account not selected
if ($option=="Select one"){
	echo "<META HTTP-EQUIV=\"Refresh\" CONTENT=\"1;	URL=reimburse.php?m=error_account\">";	
}

#if user not selected
if ($name=="Select a user"){
	echo "<META HTTP-EQUIV=\"Refresh\" CONTENT=\"1;	URL=reimburse.php?m=error_user\">";	
}

#if a cashbox transaction
if ($option=="Cashbox"){
	#Check if amount reimbursed exceeds amount owed to user
	if ($amount>$users->get_user('money_owed',$name)){
		$owed=$users->get_user('money_owed',$name);
		echo "<META HTTP-EQUIV=\"Refresh\" CONTENT=\"1;	URL=reimburse.php?m=too_much&amount=$owed\">";
	}
	else{
		#Check if balance is enough
		if ($amount>$finance->check_cashbox_balance()){
			echo "<META HTTP-EQUIV=\"Refresh\" CONTENT=\"1;	URL=reimburse.php?m=not_enough&account=cashbox\">";
		}
		else{
			#update money owed to user
			$amount1="-".$amount;
			if ($users->update_money_owed($amount1,$name)){
				#update cashbox
				$total=$finance->check_cashbox_balance()-$amount;
				$type="Reimbursement";
				if ($finance->update_cashbox($total, $amount, $type)){
					$balance=$finance->check_cashbox_balance();
					$transaction_id="cb".$finance->check_cashbox_id();
					#set the expense as administrative
						if ($finance->enter_admin_expense($amount, $type, $id, $transaction_id, $details)){
							echo "<META HTTP-EQUIV=\"Refresh\" CONTENT=\"1;	URL=reimburse.php?m=success&balance=$balance\">";
						}
				}
			}
		}
	}
}
else if($option=="Bank of Baroda account"){
	#Check if amount reimbursed exceeds amount owed to user
	if ($amount>$users->get_user('money_owed',$name)){
		$owed=$users->get_user('money_owed',$name);
		echo "<META HTTP-EQUIV=\"Refresh\" CONTENT=\"1;	URL=reimburse.php?m=too_much&amount=$owed\">";
	}
	else{
		#Check if balance is enough
		if ($amount>$finance->check_bank_baroda_balance()){
			echo "<META HTTP-EQUIV=\"Refresh\" CONTENT=\"1;	URL=reimburse.php?m=not_enough&account=baroda\">";
		}
		else{
			#update money owed to user
			$amount1="-".$amount;
			if ($users->update_money_owed($amount1,$name)){
				#update cashbox
				$total=$finance->check_bank_baroda_balance()-$amount;
				$type="Reimbursement";
				if ($finance->update_bank_baroda($total, $amount, $type)){
					$balance=$finance->check_bank_baroda_balance();
					$transaction_id="cb".$finance->check_bank_baroda_id();
					#set the expense as administrative
						if ($finance->enter_admin_expense($amount, $type, $id, $transaction_id, $details)){
							echo "<META HTTP-EQUIV=\"Refresh\" CONTENT=\"1;	URL=reimburse.php?m=success&balance=$balance\">";
						}
				}
			}
		}
	}
}
else if($option=="HFCK Bank account"){
	#Check if amount reimbursed exceeds amount owed to user
	if ($amount>$users->get_user('money_owed',$name)){
		$owed=$users->get_user('money_owed',$name);
		echo "<META HTTP-EQUIV=\"Refresh\" CONTENT=\"1;	URL=reimburse.php?m=too_much&amount=$owed\">";
	}
	else{
		#Check if balance is enough
		if ($amount>$finance->check_bank_hfck_balance()){
			echo "<META HTTP-EQUIV=\"Refresh\" CONTENT=\"1;	URL=reimburse.php?m=not_enough&account=HFCK\">";
		}
		else{
			#update money owed to user
			$amount1="-".$amount;
			if ($users->update_money_owed($amount1,$name)){
				#update cashbox
				$total=$finance->check_bank_hfck_balance()-$amount;
				$type="Reimbursement";
				if ($finance->update_bank_hfck($total, $amount, $type)){
					$balance=$finance->check_bank_hfck_balance();
					$transaction_id="cb".$finance->check_bank_hfck_id();
					#set the expense as administrative
						if ($finance->enter_admin_expense($amount, $type, $id, $transaction_id, $details)){
							echo "<META HTTP-EQUIV=\"Refresh\" CONTENT=\"1;	URL=reimburse.php?m=success&balance=$balance\">";
						}
				}
			}
		}
	}
}

else if($option=="Rafiki DTM account"){
	#Check if amount reimbursed exceeds amount owed to user
	if ($amount>$users->get_user('money_owed',$name)){
		$owed=$users->get_user('money_owed',$name);
		echo "<META HTTP-EQUIV=\"Refresh\" CONTENT=\"1;	URL=reimburse.php?m=too_much&amount=$owed\">";
	}
	else{
		#Check if balance is enough
		if ($amount>$finance->check_bank_rafiki_balance()){
			echo "<META HTTP-EQUIV=\"Refresh\" CONTENT=\"1;	URL=reimburse.php?m=not_enough&account=rafiki\">";
		}
		else{
			#update money owed to user
			$amount1="-".$amount;
			if ($users->update_money_owed($amount1,$name)){
				#update cashbox
				$total=$finance->check_bank_rafiki_balance()-$amount;
				$type="Reimbursement";
				if ($finance->update_bank_rafiki($total, $amount, $type)){
					$balance=$finance->check_bank_rafiki_balance();
					$transaction_id="cb".$finance->check_bank_rafiki_id();
					#set the expense as administrative
						if ($finance->enter_admin_expense($amount, $type, $id, $transaction_id, $details)){
							echo "<META HTTP-EQUIV=\"Refresh\" CONTENT=\"1;	URL=reimburse.php?m=success&balance=$balance\">";
						}
				}
			}
		}
	}
}
?>
