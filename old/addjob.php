<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link rel="stylesheet" href="style.css" type="text/css" />
<title>Finance System</title>
<style type="text/css">
<!--
.style1 {
	color: #0066CC;
	font-size: 24px;
	font-weight: bold;
}
-->
</style>
</head>

<body>

<div class="content">
<p align="center" class="style1">ADD A NEW JOB</p>
<?php
#set all variables from the form
	include "finance_config.php";
	include "lib/jobs.php";
	$jobs = new jobs;
	$card_id=$_POST['job_card'];
	$particulars=$_POST['particulars'];
	$quantity=$_POST['quantity'];
	$amount=$_POST['amount'];
	$total=$quantity*$amount;
#add new job card for the client
if($jobs->add_job($card_id,$particulars,$quantity,$amount)){
	if($jobs->update_card_total($card_id,$total)){
		$vat_total=(0.16)*$total+$total;
		$jobs->update_invoice_amount($card_id,$vat_total);
		echo "<p style='color:#FFFFFF'>Success</p>";
}
}
else{
	echo "<p style='color:#FFFFFF'>Something went wrong</p>";
}
	?>
<p align="center"><a class="button" href="view_client.php">Back to Client</a><a class="button" href="index.php">Back to Homepage</a></p>
<p align="center">&nbsp;</p>
</div>
</body>
</html>

