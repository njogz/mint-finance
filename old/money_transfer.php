<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link rel="stylesheet" href="style.css" type="text/css" />
<title>Finance System</title>

</head>

<body>

<div class="content">
<?php
include "finance_config.php";
include "lib/finance.php";
$finance = new finance;
$from=mysql_real_escape_string(trim($_POST['from']));
$to=mysql_real_escape_string(trim($_POST['to']));
$amount=mysql_real_escape_string(trim($_POST['amount']));

$amount=filter_var($amount, FILTER_SANITIZE_STRING);
$to=filter_var($to, FILTER_SANITIZE_STRING);
$from=filter_var($from, FILTER_SANITIZE_STRING);

$userid=$_COOKIE['id'];

if ($from==$to){
	echo "<p align='center' class='style1'><span style='color:red'>Error: Trying to transfer to and from the same account</span> \n Please close this window and try again
		</p>";
}
else{
	if ($from==1){
		if ($finance->check_cashbox_balance()<$amount){
			echo "<p align='center' class='style1'><span style='color:red'>Error: Trying to transfer more than is in account</span> \n Please close this window and try again</p>";
		}
		else{
			$cashbox_total = $finance->check_cashbox_balance()-$amount;
			$cashbox_type = "Deduction";
			if ($finance->update_cashbox($cashbox_total,$amount,$cashbox_type)){
				if($to==2){
					$rafiki_total =  $finance->check_bank_rafiki_balance()+$amount;
					$rafiki_type = "Addition from cashbox";
					if ($finance->update_bank_rafiki($rafiki_total, $amount, $rafiki_type)){
						echo "<p align='center' class='style1'>Transfer successful \n Please close this window.</p>";
					}
				}
				elseif($to==3){
					$hfck_total = $finance->check_bank_hfck_balance()+$amount;
					$hfck_type = "Addition from cashbox";
					if ($finance->update_bank_hfck($hfck_total, $amount, $hfck_type)){
						echo "<p align='center' class='style1'>Transfer successful \n Please close this window.</p>";
					}
				}
				elseif($to==4){
					$baroda_total = $finance->check_bank_baroda_balance()+$amount;
					$baroda_type = "Addition from cashbox";
					if ($finance->update_bank_baroda($baroda_total, $amount, $baroda_type)){
						echo "<p align='center' class='style1'>Transfer successful \n Please close this window.</p>";
					}
				}
			}
		}
	}
	elseif ($from==2){
		if ($finance->check_bank_rafiki_balance()<$amount){
			echo "<p align='center' class='style1'><span style='color:red'>Error: Trying to transfer more than is in account</span> \n Please close this window and try again</p>";
		}
		else{
			$rafiki_total = $finance->check_bank_rafiki_balance()-$amount;
			$rafiki_type = "Deduction";
			if ($finance->update_bank_rafiki($rafiki_total,$amount,$rafiki_type)){
				if($to==3){
					$hfck_total = $finance->check_bank_hfck_balance()+$amount;
					$hfck_type = "Addition from rafiki";
					if ($finance->update_bank_hfck($hfck_total, $amount, $hfck_type)){
						echo "<p align='center' class='style1'>Transfer successful \n Please close this window.</p>";
					}
				}
				elseif($to==4){
					$baroda_total = $finance->check_bank_baroda_balance()+$amount;
					$baroda_type = "Addition from rafiki";
					if ($finance->update_bank_baroda($baroda_total, $amount, $baroda_type)){
						echo "<p align='center' class='style1'>Transfer successful \n Please close this window.</p>";
					}
				}
				elseif($to==1){
					$cashbox_total = $finance->check_cashbox_balance()+$amount;
					$cashbox_type="Addition from rafiki";
					if ($finance->update_cashbox($cashbox_total, $amount, $cashbox_type)){
						echo "<p align='center' class='style1'>Transfer successful \n Please close this window.</p>";
					}
				}
			}
		}
	}
	elseif ($from==3){
		if ($finance->check_bank_hfck_balance()<$amount){
			echo "<p align='center' class='style1'><span style='color:red'>Error: Trying to transfer more than is in account</span> \n Please close this window and try again</p>";
		}
		else{
			$hfck_total = $finance->check_bank_hfck_balance()-$amount;
			$hfck_type = "Deductuon";
			if ($finance->update_bank_hfck($hfck_total,$amount,$hfck_type)){
				if($to==4){
					$baroda_total = $finance->check_bank_baroda_balance()+$amount;
					$baroda_type = "Addition from hfck";
					if ($finance->update_bank_baroda($baroda_total, $amount, $baroda_type)){
						echo "<p align='center' class='style1'>Transfer successful \n Please close this window.</p>";
					}
				}
				elseif($to==1){
					$cashbox_total = $finance->check_cashbox_balance()+$amount;
					$cashbox_type="Addition from hfck";
					if ($finance->update_cashbox($cashbox_total, $amount, $cashbox_type)){
						echo "<p align='center' class='style1'>Transfer successful \n Please close this window.</p>";
					}
				}
				elseif($to==2){
					$rafiki_total =  $finance->check_bank_rafiki_balance()+$amount;
					$rafiki_type = "Addition from hfck";
					if ($finance->update_bank_rafiki($rafiki_total, $amount, $rafiki_type)){
						echo "<p align='center' class='style1'>Transfer successful \n Please close this window.</p>";
					}
				}
			}
		}
		
	}
	elseif ($from==4){
		if ($finance->check_bank_baroda_balance()<$amount){
			echo "<p align='center' class='style1'><span style='color:red'>Error: Trying to transfer more than is in account</span> \n Please close this window and try again</p>";
		}
		else{
			$baroda_total = $finance->check_bank_baroda_balance()-$amount;
			$baroda_type = "Deduction";
			if ($finance->update_bank_baroda($baroda_total,$amount,$baroda_type)){
				if($to==1){
					$cashbox_total = $finance->check_cashbox_balance()+$amount;
					$cashbox_type="Addition from baroda";
					if ($finance->update_cashbox($cashbox_total, $amount, $cashbox_type)){
						echo "<p align='center' class='style1'>Transfer successful \n Please close this window.</p>";
					}
				}
				elseif($to==2){
					$rafiki_total =  $finance->check_bank_rafiki_balance()+$amount;
					$rafiki_type = "Addition from baroda";
					if ($finance->update_bank_rafiki($rafiki_total, $amount, $rafiki_type)){
						echo "<p align='center' class='style1'>Transfer successful \n Please close this window.</p>";
					}
				}
				elseif($to==3){
					$hfck_total = $finance->check_bank_hfck_balance()+$amount;
					$hfck_type = "Addition from baroda";
					if ($finance->update_bank_hfck($hfck_total, $amount, $hfck_type)){
						echo "<p align='center' class='style1'>Transfer successful \n Please close this window.</p>";
					}
				}
			}
		}
	}
	else{
		echo "<p align='center' class='style1'>An error occurred \n Please close this window and try again
		</p>";
	}
}

?>
<p align="center">&nbsp;</p>

<p align="center">&nbsp;</p>
</div>
</body>
</html>
