<?php
$user=$_COOKIE['id'];
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link href="style.css" rel="stylesheet" type="text/css"  />
<title>Finance System</title>
</head>
<body>
<div class="content">
<?php
#set all the varibles from the form
include "finance_config.php";
include "lib/users.php";
include "lib/finance.php";
$finance = new finance;
$users = new users;
$option=$_POST['option'];
$amount=$_POST['cash'];
$type="Payable payment";

$id=$_GET['id'];

$userid=$_COOKIE['id'];
#if a cashbox transaction
if ($option=="REMOVE FROM CASHBOX"){
	#Check if balance is enough
	if ($amount>$finance->check_cashbox_balance()){
	echo "<p align='center' class='style1'>The cashbox does not have enough money to handle this transaction</p>";
	}
	else{
		#update cashbox
		$total=$finance->check_cashbox_balance()-$amount;
		if ($finance->update_cashbox($total, $amount, $type)){
			echo "<p align='center' class='style1'>New Balance: ".$finance->check_cashbox_balance()."</p>";
			$transaction_id="cb".$finance->check_cashbox_id();
			#update the payable
			if($finance->pay_payable($amount,$id)){
				echo "<p align='center' class='style1'>Payable payment added</p>";
			}
		}
	}
}
else if($option=="Bank of Baroda account"){
	#Check if balance is enough
	if ($amount>$finance->check_bank_baroda_balance()){
	echo "<p align='center' class='style1'>The account does not have enough money to handle this transaction</p>";
	}
	else{
		#update account
		$total=$finance->check_bank_baroda_balance()-$amount;
		//$type="Deduction";
		if ($finance->update_bank_baroda($total, $amount, $type)){
			echo "New Balance: ".$finance->check_bank_baroda_balance();
			$transaction_id="bb".$finance->check_bank_baroda_id();
			#update the payable
			if($finance->pay_payable($amount,$id)){
				echo "<p align='center' class='style1'>Payable payment added</p>";
			}
		}
	}
}
else if($option=="HFCK Bank account"){
	#Check if balance is enough
	if ($amount>$finance->check_bank_hfck_balance()){
	echo "<p align='center' class='style1'>The account does not have enough money to handle this transaction</p>";
	}
	else{
		#update account
		$total=$finance->check_bank_hfck_balance()-$amount;
		//$type="Deduction";
		if ($finance->update_bank_hfck($total, $amount, $type)){
			echo "New Balance: ".$finance->check_bank_hfck_balance();
			$transaction_id="bh".$finance->check_bank_hfck_id();
			#update the payable
			if($finance->pay_payable($amount,$id)){
				echo "<p align='center' class='style1'>Payable payment added</p>";
			}
		}
	}
}
else if($option=="Rafiki DTM account"){
	#Check if balance is enough
	if ($amount>$finance->check_bank_rafiki_balance()){
	echo "<p align='center' class='style1'>The account does not have enough money to handle this transaction</p>";
	}
	else{
		#update account
		$total=$finance->check_bank_rafiki_balance()-$amount;
		//$type="Deduction";
		if ($finance->update_bank_rafiki($total, $amount, $type)){
			echo "New Balance: ".$finance->check_bank_rafiki_balance();
			$transaction_id="br".$finance->check_bank_rafiki_id();
			#update the payable
			if($finance->pay_payable($amount,$id)){
				echo "<p align='center' class='style1'>Payable payment added</p>";
			}
		}
	}
}
else if($option=="Personal Money"){
	#update money owed to user
	if ($users->update_money_owed($amount, $userid)){
		echo "You are currently owed: ".$users->get_user("money_owed",$userid);
		$transaction_id="user".$userid;
		#update the payable
			if($finance->pay_payable($amount,$id)){
				echo "<p align='center' class='style1'>Payable payment added</p>";
			}
	}	
}
?>
  <p align="center">&nbsp;</p>
<p align="center">&nbsp;</p>
</div>
</body>
</html>
