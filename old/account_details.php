<?php
if (isset($_COOKIE['id'])) {
}
else{
header("location:login.php");
}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link href="style.css" rel="stylesheet" type="text/css"  />
<title>Finance System</title>

</head>

<body background="images/bg.JPG">

<div class="content">
<p align="center" class="style1">ACCOUNT DETAILS</p>
<p align="center">
  <?php
	include "lib/finance.php";
	$finance = new finance;
	include "finance_config.php";
echo "<table width='90%' border='1' cellspacing='0' cellpadding='0'>";
 echo "<tr>";
    echo "<th width='20%'><b>Name</b></th>";
    echo "<td width='20%'align='center'><b>Current Balance</b></td>";
  echo "</tr>";
  echo "<tr>";
    echo "<th width='20%'>Bank of Baroda account</th>";
    echo "<td width='20%'align='center'>".$finance->check_bank_baroda_balance()."</td>";
  echo "</tr>";
  echo "<tr>";
    echo "<th width='20%'>HFCK Bank account</th>";
    echo "<td width='20%'align='center'>".$finance->check_bank_hfck_balance()."</td>";
  echo "</tr>";
  echo "<tr>";
    echo "<th width='20%'>Rafiki DTM account</th>";
    echo "<td width='20%'align='center'>".$finance->check_bank_rafiki_balance()."</td>";
  echo "</tr>";
  echo "<tr>";
    echo "<th width='20%'>Cashbox</th>";
    echo "<td width='20%'align='center'>".$finance->check_cashbox_balance()."</td>";
  echo "</tr>";
  echo "<tr>";
    echo "<th width='20%'><b>Total</b></th>";
    $total=$finance->check_cashbox_balance()+$finance->check_bank_rafiki_balance()+$finance->check_bank_hfck_balance()+$finance->check_bank_baroda_balance();
    echo "<td width='20%'align='center'><b>".$total."</b></td>";
  echo "</tr>";
echo "</table>";
?>
</p>

<p align="center"><a class="button" href="<?php echo "edit_job.php?id=$job->job_id"; ?>" onclick="window.open('<?php echo "transfer_money.php"; ?>','popup','width=550,height=400,scrollbars=no,resizable=no,toolbar=no,directories=no,location=no,menubar=no,status=no,left=0,top=0');return false">Transfer Cash</a><a class="button" href="index.php">Back to homepage</a><a class="button" href="finance.php">Back</a></p></div>
</body>
</html>
