<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link href="style.css" rel="stylesheet" type="text/css"  />
<title>Finance System</title>

</head>

<body background="../images/bg.JPG">

<div class="content">
<p align="center">
<?php
include "finance_config.php";
include "lib/jobs.php";
$jobs = new jobs;
$id=$_GET['id'];
$query="SELECT * FROM job_card WHERE card_id=$id";
$result=mysql_query($query);
$job_card=mysql_fetch_object($result);

echo "<p align='center' class='style1'>".$job_card->name." Details</p>";
echo "<table width='0' style='float:left' border='0' cellspacing='10' cellpadding='0' align='center'>";
echo "<tr>";
echo "<th scope='row' align='right'>DATE STARTED:</th>";
echo "<td>".$job_card->date_created."</td>";
  echo "</tr>";
  echo "<tr>";
    echo "<th scope='row' align='right'>STATUS:</th>";
    echo "<td>".$job_card->status."</td>";
  echo "</tr>";
  echo "<tr>";
    echo "<th scope='row' align='right'>COMMENTS:</th>";
    echo "<td>".$job_card->comments."</td>";
  echo "</tr>";
    echo "<tr>";
    echo "<th scope='row' align='right'>TOTAL (inc VAT):</th>";
    $total=$jobs->get_card_field('total', $id);
    $vat_total=(0.16*$total)+$total;
    echo "<td>".number_format($vat_total,2)."</td>";
  echo "</tr>";
    echo "<tr>";
    echo "<th scope='row' align='right'>TOTAL EXPENSES:</th>";
    echo "<td>".number_format($jobs->get_card_expenses($id),2)."</td>";
  echo "</tr>";
    echo "</tr>";
    echo "<tr>";
    echo "<th scope='row' align='right'><span style='color:green'>PROFIT</span>/<span style='color:red'>LOSS</span>:</th>";
    $difference=$vat_total-$jobs->get_card_expenses($id);
    if($difference<0){
		$profit="<b style='color:red'>".number_format($difference,2)."</b>";
	}
	else{
		$profit="<b style='color:green'>".number_format($difference,2)."</b>";
	}
    echo "<td>".$profit."</td>";
  echo "</tr>";
  echo "<tr>";
    echo "<th scope='row' align='right'>AMOUNT PAID:</th>";
    echo "<td>".number_format($jobs->get_card_field('amount_paid', $id),2)."</td>";
  echo "</tr>";
$query2="SELECT * FROM job WHERE card_id=$id";
$result2=mysql_query($query2);
echo "<a href='add_job.php?id=$id'><h3>ADD JOB</h3></a>";
#display jobs associated with this job card
echo "<table border='1' cellspacing='0' cellpadding='0'  align='center'>";
 echo "<tr>";
	echo "<th width='200px'><b>Particulars</b></th>";
    echo "<td width='70px'><b>Quantity</b></td>";
    echo "<td width='70px'><b>Amount</b></td>";
    echo "<td width='200px'><b>Add stock used</b></td>";
    echo "<td width='70px'><b>Edit</b></td>";
  echo "</tr>";

while ($job=mysql_fetch_object($result2)){
	echo "<tr>";
    echo "<th width='200px'><b>".$job->particulars."</b></th>";
	echo "<td width='100px'><b>".$job->quantity."</b></td>";
    echo "<td width='100px'><b>".$job->amount."</b></td>";
	echo "<td width='200px'>"; 
	if ($jobs->get_card_status($id)=="incomplete"){?> <a href="<?php echo "deduct_stock.php?id=$job->job_id"; ?>" onclick="window.open('<?php echo "deduct_stock.php?id=$job->job_id"; ?>','popup','width=550,height=400,scrollbars=no,resizable=no,toolbar=no,directories=no,location=no,menubar=no,status=no,left=0,top=0');return false">ADD STOCK USED</a><?php } 
	#remove link if job is complete
	else { echo "Add stock used"; } "</td>";
	echo "<td width='100px'>";
	?> <a href="<?php echo "edit_job.php?id=$job->job_id"; ?>" onclick="window.open('<?php echo "edit_job.php?id=$job->job_id"; ?>','popup','width=550,height=400,scrollbars=no,resizable=no,toolbar=no,directories=no,location=no,menubar=no,status=no,left=0,top=0');return false">Edit</a><?php
	echo "</td>";
  echo "</tr>";
}
echo "</table>";
echo "</table>";
/*
if ($job->amount_paid==$job->total){
$query="UPDATE jobs SET status='complete' WHERE job_id=$job->job_id ";
mysql_query($query);
$query2="UPDATE invoices SET status='paid' WHERE job_id=$job->job_id";
mysql_query($query2);
}
*/
?>
</p>
<p style="clear:both"></p>

<p align="center">
<?php echo "<a class='button' href='client.php?id=$job_card->client_id'>"; ?>Back<?php echo "</a>"; ?></p>
</div>
</body>
</html>
