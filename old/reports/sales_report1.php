<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Finance System</title>
<link rel="stylesheet" href="../style.css" type="text/css" />
</head>

<body>

<div class="content">
<p align="center">
<?php
include "../finance_config.php";
include "../lib/users.php";
$user = new users;
$query="SELECT * FROM job_card ORDER BY date_created DESC";
$result=mysql_query($query);
echo "<p align='center' class='style1'>SALES REPORT</p>";
echo "<table width='700' border='1' cellspacing='2' cellpadding='0' align='center'>";
  echo "<tr>";
    echo "<th width='16%' scope='row' align='center'><b>Date</b></th>";
    echo "<td width='16%' align='center'><b>Job Name</b></td>";
    echo "<td width='16%' align='center'><b>Client Name</b></td>";
    echo "<td width='16%' align='center'><b>Amount</b></td>";
    echo "<td width='16%' align='center'><b>Amount Paid</b></td>";
    echo "<td width='16%' align='center'><b>Amount owed</b></td>";
  echo "</tr>";
echo "</table>";

while ($sales=mysql_fetch_object($result)){
echo "<table width='700' border='1' cellspacing='2' cellpadding='0' align='center'>";
  echo "<tr>";
    echo "<th width='16%' scope='row' align='center'>".$sales->date_created."</th>";
    echo "<td width='16%' align='center'>".$sales->name."</td>";
    echo "<td width='16%' align='center'>".$user->get_client('client_name',$sales->client_id)."</td>";
    echo "<td width='16%' align='center'>".$sales->total."</td>";
    echo "<td width='16%' align='center'>".$sales->amount_paid."</td>";
	$amount_owed=$sales->total-$sales->amount_paid;
    echo "<td width='16%' align='center'>".$amount_owed."</td>";
  echo "</tr>";
echo "</table>";
}
$query="SELECT SUM(total) AS total FROM job_card";
$result=mysql_query($query);
while ($total=mysql_fetch_object($result)){
$salestotal=$total->total;
}
$query="SELECT SUM(amount_paid) AS paid FROM job_card";
$result=mysql_query($query);
while ($total=mysql_fetch_object($result)){
$paid=$total->paid;
}
$owed=$salestotal-$paid;
echo "<table width='600' border='1' cellspacing='2' cellpadding='0' align='center'>";
  echo "<tr>";
    echo "<th width='33%' scope='row' align='center'><b>TOTALS</b></th>";
	echo "<td width='10%' align='center'><b>".number_format($salestotal,2)."</b></td>";
    echo "<td width='10%' align='center'><b>".number_format($paid,2)."</b></td>";
    echo "<td width='10%' align='center'><b>".number_format($owed,2)."</b></td>";
  echo "</tr>";
echo "</table>";

?>
</p>
<p align="center"><a class="button" href="sales_report_print.php">Print</a><a class="button" href="../reports.php">Back</a></p>
</div>
</body>
</html>