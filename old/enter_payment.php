<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link rel="stylesheet" href="style.css" type="text/css" />
<title>Finance System</title>

</head>

<body>

<div class="content">
<?php
include "finance_config.php";
include "lib/finance.php";
include "lib/jobs.php";
$jobs = new jobs;
$finance = new finance;
$option=$_POST['option'];
$cash=$_POST['cash'];
$details=$_POST['details'];
$id=$_GET['id'];
$userid=$_COOKIE['id'];
$date=date("d-m-Y");
#check that amount entered is not more than total
$select="SELECT * FROM job_card WHERE card_id=$id";
$selectall=mysql_query($select);
$job=mysql_fetch_object($selectall);
$client_id=$job->client_id;
$client_name=$jobs->get_client_details('client_name',$client_id);
if ($id==0){
	$type="Misc income";
}
else{
	$type=$client_name;
}
if ($id==0){
$amountpaid=0;
$amountpayable=1;
}
else{
$amountpaid=$job->amount_paid+$cash;
$amountpayable=1.16*($job->total);
$amountpaid=(int)$amountpaid;
$amountpayable=(int)$amountpayable;
}
if ($amountpaid>$amountpayable){
echo "<p align='center' class='style1'>The amount should not be more than Ksh".$amountpayable." \n Please close this window and enter again</p>";
}

else{
if ($option=="ADD TO CASHBOX"){
	#update the cashbox
	$total=$finance->check_cashbox_balance()+$cash;
	if($finance->update_cashbox($total, $cash, $type)){
		#keep record of payment
		$transaction_id="cb".$finance->check_cashbox_id();
		if ($id==0){
		if($finance->enter_payment1($cash,$id,$transaction_id,$details)){
				#update job card payment records
					echo "<p align='center' class='style1'>NEW BALANCE:".$finance->check_cashbox_balance()."</p>";
					echo "<p align='center' class='style1'>Misc Income added \n Please close this window</p>";
			}
		}
		else{
			if($finance->enter_payment($cash,$id,$transaction_id)){
				#update job card payment records
				if($jobs->update_card_paid($id,$cash)){
					echo "NEW BALANCE:".$finance->check_cashbox_balance();
					echo "<p align='center' class='style1'>The cashbox has been updated \n Please close this window</p>";
				}
			}
		}
	}
}
else if ($option=="Bank of Baroda account"){
	#update bank account
	$total=$finance->check_bank_baroda_balance()+$cash;
	if($finance->update_bank_baroda($total, $cash, $type)){
		#keep record of payment
		$transaction_id="bb".$finance->check_bank_baroda_id();
		if ($id==0){
		if($finance->enter_payment1($cash,$id,$transaction_id,$details)){
				#update job card payment records
					echo "NEW BALANCE:".$finance->check_cashbox_balance();
					echo "<p align='center' class='style1'>Misc Income added \n Please close this window</p>";
			}
		}
		else{
			if($finance->enter_payment($cash,$id,$transaction_id)){
				#update job card payment records
				if($jobs->update_card_paid($id,$cash)){
					echo "NEW BALANCE:".$finance->check_bank_baroda_balance();
					echo "<p align='center' class='style1'>The Bank of Baroda account has been updated \n Please close this window</p>";
				}
			}
		}
	}
}
else if ($option=="HFCK Bank account"){
	#update bank account
	$total=$finance->check_bank_hfck_balance()+$cash;
	if($finance->update_bank_hfck($total, $cash, $type)){
		#keep record of payment
		$transaction_id="bh".$finance->check_bank_hfck_id();
		if ($id==0){
		if($finance->enter_payment1($cash,$id,$transaction_id,$details)){
				#update job card payment records
					echo "NEW BALANCE:".$finance->check_cashbox_balance();
					echo "<p align='center' class='style1'>Misc Income added \n Please close this window</p>";
			}
		}
		else{
			if($finance->enter_payment($cash,$id,$transaction_id)){
				#update job card payment records
				if($jobs->update_card_paid($id,$cash)){
					echo "NEW BALANCE:".$finance->check_bank_hfck_balance();
					echo "<p align='center' class='style1'>The HFCK Bank account has been updated \n Please close this window</p>";
				}
			}
		}
	}
}
else if ($option=="Rafiki DTM account"){
	#update bank account
	$total=$finance->check_bank_rafiki_balance()+$cash;
	if($finance->update_bank_rafiki($total, $cash, $type)){
		#keep record of payment
		$transaction_id="br".$finance->check_bank_rafiki_id();
		if ($id==0){
		if($finance->enter_payment1($cash,$id,$transaction_id,$details)){
				#update job card payment records
					echo "NEW BALANCE:".$finance->check_cashbox_balance();
					echo "<p align='center' class='style1'>Misc Income added \n Please close this window</p>";
			}
		}
		else{
			if($finance->enter_payment($cash,$id,$transaction_id)){
				#update job card payment records
				if($jobs->update_card_paid($id,$cash)){
					echo "NEW BALANCE:".$finance->check_bank_rafiki_balance();
					echo "<p align='center' class='style1'>The Rafiki DTM account has been updated \n Please close this window</p>";
				}
			}
		}
	}
}
else{
echo "<p align='center' class='style1'>Something went wrong \n Please close this window and try again</p>";
}
}
?>
  <p align="center">&nbsp;</p>

<p align="center">&nbsp;</p>
</div>
</body>
</html>
