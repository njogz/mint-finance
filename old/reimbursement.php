<?php
$user=$_COOKIE['id'];
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link href="style.css" rel="stylesheet" type="text/css"  />
<title>Finance System</title>

</head>

<body>

<div class="content">
<?php
#set all the varibles from the form
include "finance_config.php";
include "lib/users.php";
include "lib/finance.php";
$finance = new finance;
$users = new users;
$amount=$_POST['cash'];
$date=date("d-m-Y");
$name=$_POST['user'];
$userid=$user;
$details=$_POST['details'];
$option=$_POST['option2'];

#if a cashbox transaction
if ($option=="REMOVE FROM CASHBOX"){
	#Check if amount reimbursed exceeds amount owed to user
	if ($amount>$users->get_user('money_owed',$name)){
		echo "<p align='center' class='style1'>Maximum amount reimbursable to this user is Ksh".$users->get_user('money_owed',$name)."</p>";
	}
	else{
		#Check if balance is enough
		if ($amount>$finance->check_cashbox_balance()){
			echo "<p align='center' class='style1'>The cashbox does not have enough money to handle this transaction</p>";
		}
		else{
			#update money owed to user
			$amount1="-".$amount;
			if ($users->update_money_owed($amount1,$name)){
				#update cashbox
				$total=$finance->check_cashbox_balance()-$amount;
				$type="Reimbursement";
				if ($finance->update_cashbox($total, $amount, $type)){
					echo "New Balance: ".$finance->check_cashbox_balance();
					$transaction_id="cb".$finance->check_cashbox_id();
					#set the expense as administrative
						if ($finance->enter_admin_expense($amount, $type, $id, $transaction_id, $details)){
							echo "<p align='center' class='style1'>Reimbursement made. We good</p>";
						}
				}
			}
		}
	}
}
else if($option=="Bank of Baroda account"){
	#Check if amount reimbursed exceeds amount owed to user
	if ($amount>$users->get_user('money_owed',$name)){
		echo "<p align='center' class='style1'>Maximum amount reimbursable to this user is Ksh".$users->get_user('money_owed',$name)."</p>";
	}
	else{
		#Check if balance is enough
		if ($amount>$finance->check_bank_baroda_balance()){
			echo "<p align='center' class='style1'>This account does not have enough money to handle this transaction</p>";
		}
		else{
			#update money owed to user
			$amount1="-".$amount;
			if ($users->update_money_owed($amount1,$name)){
				#update cashbox
				$total=$finance->check_bank_baroda_balance()-$amount;
				$type="Reimbursement";
				if ($finance->update_bank_baroda($total, $amount, $type)){
					echo "New Balance: ".$finance->check_bank_baroda_balance();
					$transaction_id="cb".$finance->check_bank_baroda_id();
					#set the expense as administrative
						if ($finance->enter_admin_expense($amount, $type, $id, $transaction_id, $details)){
							echo "<p align='center' class='style1'>Reimbursement made. We good</p>";
						}
				}
			}
		}
	}
}
else if($option=="HFCK Bank account"){
	#Check if amount reimbursed exceeds amount owed to user
	if ($amount>$users->get_user('money_owed',$name)){
		echo "<p align='center' class='style1'>Maximum amount reimbursable to this user is Ksh".$users->get_user('money_owed',$name)."</p>";
	}
	else{
		#Check if balance is enough
		if ($amount>$finance->check_bank_hfck_balance()){
			echo "<p align='center' class='style1'>This account does not have enough money to handle this transaction</p>";
		}
		else{
			#update money owed to user
			$amount1="-".$amount;
			if ($users->update_money_owed($amount1,$name)){
				#update cashbox
				$total=$finance->check_bank_hfck_balance()-$amount;
				$type="Reimbursement";
				if ($finance->update_bank_hfck($total, $amount, $type)){
					echo "New Balance: ".$finance->check_bank_hfck_balance();
					$transaction_id="cb".$finance->check_bank_hfck_id();
					#set the expense as administrative
						if ($finance->enter_admin_expense($amount, $type, $id, $transaction_id, $details)){
							echo "<p align='center' class='style1'>Reimbursement made. We good</p>";
						}
				}
			}
		}
	}
}

else if($option=="Rafiki DTM account"){
	#Check if amount reimbursed exceeds amount owed to user
	if ($amount>$users->get_user('money_owed',$name)){
		echo "<p align='center' class='style1'>Maximum amount reimbursable to this user is Ksh".$users->get_user('money_owed',$name)."</p>";
	}
	else{
		#Check if balance is enough
		if ($amount>$finance->check_bank_rafiki_balance()){
			echo "<p align='center' class='style1'>This account does not have enough money to handle this transaction</p>";
		}
		else{
			#update money owed to user
			$amount1="-".$amount;
			if ($users->update_money_owed($amount1,$name)){
				#update cashbox
				$total=$finance->check_bank_rafiki_balance()-$amount;
				$type="Reimbursement";
				if ($finance->update_bank_rafiki($total, $amount, $type)){
					echo "New Balance: ".$finance->check_bank_rafiki_balance();
					$transaction_id="cb".$finance->check_bank_rafiki_id();
					#set the expense as administrative
						if ($finance->enter_admin_expense($amount, $type, $id, $transaction_id, $details)){
							echo "<p align='center' class='style1'>Reimbursement made. We good</p>";
						}
				}
			}
		}
	}
}


?>
  <p align="center">&nbsp;</p>

<p align="center">&nbsp;</p>
</div>
</body>
</html>
