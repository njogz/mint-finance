<?php
$id=$_COOKIE['id'];
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link href="style.css" rel="stylesheet" type="text/css"  />
<title>Finance System</title>
</head>
<body>
<div class="content">
<?php
#set all the varibles from the form
include "finance_config.php";
include "lib/finance.php";
include "lib/users.php";

$finance = new finance;
$users = new users;
$account=$_POST['account'];
$amount=$_POST['amount'];
$loanid=$_POST['loanid'];
$date=date("d-m-Y");
$userid=$_COOKIE['id'];

#if a cashbox transaction
if ($account=="Cashbox"){
	#update the cashbox
	$total=$finance->check_cashbox_balance()+$amount;
	if($finance->update_cashbox($total, $amount, $type)){
		#keep record of payment
		$transaction_id="cb".$finance->check_cashbox_id();
		if ($users->pay_loan_user($amount,$loanid)){
			echo "NEW BALANCE:".$finance->check_cashbox_balance();
			echo "<p align='center' class='style1'>The cashbox and the loan have been updated.</p>";
			echo "<p align='center'><a class='button' href='list_user_loans.php'>Back to loans</a> <a class='button'  href='index.php'>Home</a></p>";
		}
	}	
}
else if($account=="Bank of Baroda account"){
	#update the account
	$total=$finance->check_bank_baroda_balance()+$amount;
	if($finance->update_bank_baroda($total, $amount, $type)){
		#keep record of payment
		$transaction_id="cb".$finance->check_bank_baroda_id();
		if ($users->pay_loan_user($amount,$loanid)){
			echo "NEW BALANCE:".$finance->check_bank_baroda_balance();
			echo "<p align='center' class='style1'>The account and the loan have been updated.</p>";
			echo "<p align='center'><a class='button' href='list_user_loans.php'>Back to loans</a> <a class='button'  href='index.php'>Home</a></p>";
		}
	}
}
else if($account=="HFCK Bank account"){
	#update the account
	$total=$finance->check_bank_hfck_balance()+$amount;
	if($finance->update_bank_hfck($total, $amount, $type)){
		#keep record of payment
		$transaction_id="cb".$finance->check_bank_hfck_id();
		if ($users->pay_loan_user($amount,$loanid)){
			echo "NEW BALANCE:".$finance->check_bank_hfck_balance();
			echo "<p align='center' class='style1'>The account and the loan have been updated.</p>";
			echo "<p align='center'><a class='button' href='list_user_loans.php'>Back to loans</a> <a class='button'  href='index.php'>Home</a></p>";
		}
	}
}

else if($account=="Rafiki DTM account"){
	#update the account
	$total=$finance->check_bank_rafiki_balance()+$amount;
	if($finance->update_bank_rafiki($total, $amount, $type)){
		#keep record of payment
		$transaction_id="cb".$finance->check_bank_rafiki_id();
		if ($users->pay_loan_user($amount,$loanid)){
			echo "NEW BALANCE:".$finance->check_bank_rafiki_balance();
			echo "<p align='center' class='style1'>The account and the loan have been updated.</p>";
			echo "<p align='center'><a class='button' href='list_user_loans.php'>Back to loans</a> <a class='button'  href='index.php'>Home</a></p>";
		}
	}
}
?>
  <p align="center">&nbsp;</p>

<p align="center">&nbsp;</p>
</div>
</body>
</html>
