<?php
if (isset($_COOKIE['id'])) {
}
else{
header("location:login.php");
}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link href="style.css" rel="stylesheet" type="text/css"  />
<title>Finance System</title>

</head>

<body background="images/bg.JPG">

<div class="content">
<p align="center" class="style1">CLIENTS</p>
<ul>
<li>
  <a class="small_button" href="new_client.php">Create new client</a>
  </li>
<li>
  <a class="small_button" href="search_invoice.php">Search Invoice</a>
  </li>
</ul>
<p align="center">
  <?php
include "finance_config.php";
echo "<table width='90%' border='1' cellspacing='0' cellpadding='0'>";
 echo "<tr>";
    echo "<th width='20%'><b>Name</b></th>";
    echo "<td width='20%'align='center'><b>Number</b></td>";
    echo "<td width='20%'align='center'><b>Company</b></td>";
    echo "<td width='20%' align='center'><b>No. Of Jobs</b></td>";
	echo "<td width='20%'align='center'><b>Edit</b></td>";
  echo "</tr>";
echo "</table>";


	$tbl_name="clients";		//your table name
	// How many adjacent pages should be shown on each side?
	$adjacents = 3;
	
	/* 
	   First get total number of rows in data table. 
	   If you have a WHERE clause in your query, make sure you mirror it here.
	*/
	$query = "SELECT COUNT(*) as num FROM $tbl_name";
	$total_pages = mysql_fetch_array(mysql_query($query));
	$total_pages = $total_pages[num];
	
	/* Setup vars for query. */
	$targetpage = "view_client.php"; 	//your file name  (the name of this file)
	$limit = 50; 								//how many items to show per page
	$page = $_GET['page'];
	if($page) 
		$start = ($page - 1) * $limit; 			//first item to display on this page
	else
		$start = 0;								//if no page var is given, set start to 0
	
	/* Get data. */
	$sql = "SELECT * FROM $tbl_name ORDER BY client_name ASC LIMIT $start, $limit";
	$result = mysql_query($sql);
	
	/* Setup page vars for display. */
	if ($page == 0) $page = 1;					//if no page var is given, default to 1.
	$prev = $page - 1;							//previous page is page - 1
	$next = $page + 1;							//next page is page + 1
	$lastpage = ceil($total_pages/$limit);		//lastpage is = total pages / items per page, rounded up.
	$lpm1 = $lastpage - 1;						//last page minus 1
	
	/* 
		Now we apply our rules and draw the pagination object. 
		We're actually saving the code to a variable in case we want to draw it more than once.
	*/
	$pagination = "";
	if($lastpage > 1)
	{	
		$pagination .= "<div class=\"pagination\">";
		//previous button
		if ($page > 1) 
			$pagination.= "<a href=\"$targetpage?page=$prev\"><< previous</a>";
		else
			$pagination.= "<span class=\"disabled\"><< previous</span>";	
		
		//pages	
		if ($lastpage < 7 + ($adjacents * 2))	//not enough pages to bother breaking it up
		{	
			for ($counter = 1; $counter <= $lastpage; $counter++)
			{
				if ($counter == $page)
					$pagination.= "<span class=\"current\">$counter</span>";
				else
					$pagination.= "<a href=\"$targetpage?page=$counter\">$counter</a>";					
			}
		}
		elseif($lastpage > 5 + ($adjacents * 2))	//enough pages to hide some
		{
			//close to beginning; only hide later pages
			if($page < 1 + ($adjacents * 2))		
			{
				for ($counter = 1; $counter < 4 + ($adjacents * 2); $counter++)
				{
					if ($counter == $page)
						$pagination.= "<span class=\"current\">$counter</span>";
					else
						$pagination.= "<a href=\"$targetpage?page=$counter\">$counter</a>";					
				}
				$pagination.= "...";
				$pagination.= "<a href=\"$targetpage?page=$lpm1\">$lpm1</a>";
				$pagination.= "<a href=\"$targetpage?page=$lastpage\">$lastpage</a>";		
			}
			//in middle; hide some front and some back
			elseif($lastpage - ($adjacents * 2) > $page && $page > ($adjacents * 2))
			{
				$pagination.= "<a href=\"$targetpage?page=1\">1</a>";
				$pagination.= "<a href=\"$targetpage?page=2\">2</a>";
				$pagination.= "...";
				for ($counter = $page - $adjacents; $counter <= $page + $adjacents; $counter++)
				{
					if ($counter == $page)
						$pagination.= "<span class=\"current\">$counter</span>";
					else
						$pagination.= "<a href=\"$targetpage?page=$counter\">$counter</a>";					
				}
				$pagination.= "...";
				$pagination.= "<a href=\"$targetpage?page=$lpm1\">$lpm1</a>";
				$pagination.= "<a href=\"$targetpage?page=$lastpage\">$lastpage</a>";		
			}
			//close to end; only hide early pages
			else
			{
				$pagination.= "<a href=\"$targetpage?page=1\">1</a>";
				$pagination.= "<a href=\"$targetpage?page=2\">2</a>";
				$pagination.= "...";
				for ($counter = $lastpage - (2 + ($adjacents * 2)); $counter <= $lastpage; $counter++)
				{
					if ($counter == $page)
						$pagination.= "<span class=\"current\">$counter</span>";
					else
						$pagination.= "<a href=\"$targetpage?page=$counter\">$counter</a>";					
				}
			}
		}
		
		//next button
		if ($page < $counter - 1) 
			$pagination.= "<a href=\"$targetpage?page=$next\">next >></a>";
		else
			$pagination.= "<span class=\"disabled\">next >></span>";
		$pagination.= "</div>\n";		
	}

		while($clients = mysql_fetch_object($result))
		{
			echo "<table width='90%' border='1' cellspacing='2' cellpadding='0'>";
			 echo "<tr>";
				echo "<th width='20%'><a href='client.php?id=$clients->client_id'>".$clients->client_name."</a></th>";
				echo "<td width='20%'align='center'>".$clients->client_number."</td>";
				echo "<td width='20%'align='center'>".$clients->company."</td>";
				echo "<td width='20%' align='center'>".$clients->jobs."</td>";
				echo "<td width='20%'align='center'><b><a href='edit_client.php?id=$clients->client_id'>Edit</b></td>";
			  echo "</tr>";
			echo "</table>";
		}
?>

<?=$pagination?>
<?php
/*$query="SELECT * FROM clients ORDER BY client_name ASC";
$result=mysql_query($query);
while ($clients=mysql_fetch_object($result)){
echo "<table width='90%' border='1' cellspacing='2' cellpadding='0'>";
 echo "<tr>";
    echo "<th width='20%'><a href='client.php?id=$clients->client_id'>".$clients->client_name."</a></th>";
    echo "<td width='20%'align='center'>".$clients->client_number."</td>";
    echo "<td width='20%'align='center'>".$clients->company."</td>";
    echo "<td width='20%' align='center'>".$clients->jobs."</td>";
	echo "<td width='20%'align='center'><b><a href='edit_client.php?id=$clients->client_id'>Edit</b></td>";
  echo "</tr>";
echo "</table>";
}*/
?>
</p>

<p align="center"><a class="button" href="index.php">Back to homepage</a></p></div>
</body>
</html>
