<?php

include("mpdf/mpdf.php");

$mpdf=new mPDF(); 

// $html must be defined
$html=file_get_contents('create_invoice.php');
$mpdf->WriteHTML($html);

$content = $mpdf->Output('', 'S');

$content = chunk_split(base64_encode($content));
$mailto = 'njgndungu@gmail.com';
$from_name = 'Mint Graphics';
$from_mail = 'accounts@mintgraphics.co.ke';
$replyto = 'accounts@mintgraphics.co.ke';
$uid = md5(uniqid(time())); 
$subject = 'Customer Invoice';
$message = 'Dear customer, Find your invoice attached';
$filename = 'invoice.pdf';

$header = "From: ".$from_name." <".$from_mail.">\n";
$header .= "Reply-To: ".$replyto."\n";
$header .= "MIME-Version: 1.0\n";
$header .= "Content-Type: multipart/mixed; boundary=\"".$uid."\"\n\n";
$header .= "This is a multi-part message in MIME format.\n";
$header .= "--".$uid."\n";
$header .= "Content-type:text/plain; charset=iso-8859-1\n";
$header .= "Content-Transfer-Encoding: 7bit\n\n";
$header .= $message."\n\n";
$header .= "--".$uid."\n";
$header .= "Content-Type: application/pdf; name=\"".$filename."\"\n";
$header .= "Content-Transfer-Encoding: base64\n";
$header .= "Content-Disposition: attachment; filename=\"".$filename."\"\n\n";
$header .= $content."\n\n";
$header .= "--".$uid."--";
$is_sent = mail($mailto, $subject, "", $header);
if ($is_sent){
	echo "success";
}
else{
	echo "try again";
}

// You can now optionally also send it to the browser
exit;

?>
