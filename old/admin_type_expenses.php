<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link rel="stylesheet" href="style.css" type="text/css" />
<title>Finance System</title>

</head>

<body>

<div class="content">
<p align="center">
<?php
include "finance_config.php";
$query="SELECT SUM(amount) FROM expenses WHERE category='admin_expense' AND TYPE='airtime'";
$result=mysql_query($query);
$row = mysql_fetch_row($result);
$airtime= $row[0];

$query1="SELECT SUM(amount) FROM expenses WHERE category='admin_expense' AND TYPE='rent'";
$result1=mysql_query($query1);
$row1 = mysql_fetch_row($result1);
$rent= $row1[0];

$query2="SELECT SUM(amount) FROM expenses WHERE category='admin_expense' AND TYPE='cleaning'";
$result2=mysql_query($query2);
$row2 = mysql_fetch_row($result2);
$cleaning= $row2[0];

$query3="SELECT SUM(amount) FROM expenses WHERE category='admin_expense' AND TYPE='stationery'";
$result3=mysql_query($query3);
$row3 = mysql_fetch_row($result3);
$stationery= $row3[0];
echo "<p align='center' class='style1'>ADMIN EXPENSES BY TYPES</p>";
?>

<div style="margin-top:1px">
<?php
echo "<table width='500' border='1' cellspacing='2' cellpadding='0' align='center'>";
  echo "<tr>";
    echo "<th width='81' scope='row' align='center'><b>Type</b></th>";
    echo "<td width='120' align='center'><b>Amount</b></td>";
  echo "</tr>";
  echo "<tr>";
    echo "<th width='81' scope='row' align='center'><b>Airtime</b></th>";
    echo "<td width='120' align='center'><b>".$airtime."</b></td>";
  echo "</tr>";
  echo "<tr>";
    echo "<th width='81' scope='row' align='center'><b>Rent</b></th>";
    echo "<td width='120' align='center'><b>".$rent."</b></td>";
  echo "</tr>";
  echo "<tr>";
    echo "<th width='81' scope='row' align='center'><b>Clenaing</b></th>";
    echo "<td width='120' align='center'><b>".$cleaning."</b></td>";
  echo "</tr>";
  echo "<tr>";
    echo "<th width='81' scope='row' align='center'><b>Stationery</b></th>";
    echo "<td width='120' align='center'><b>".$stationery."</b></td>";
  echo "</tr>";
echo "</table>";


?>
</p>
<p align="center"><a class="button" href="adminexpenses_report.php">Back</a></p>
</div>
</div>
</body>
</html>
