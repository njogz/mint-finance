<?php
$user=$_COOKIE['id'];
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link href="style.css" rel="stylesheet" type="text/css"  />
<title>Finance System</title>
</head>

<body>

<div class="content">
<?php
#set all the varibles from the form
include "finance_config.php";
include "lib/finance.php";
include "lib/users.php";
$finance = new finance;
$users = new users;
$account=$_POST['account'];
$amount=$_POST['amount'];
$date=date("d-m-Y");
$userid=$_COOKIE['id'];
#if a cashbox transaction
if ($account=="Cashbox"){
	#update cashbox
	$total=$finance->check_cashbox_balance()+$amount;
	$type="Addition";
	if ($finance->update_cashbox($total, $amount, $type)){
		echo "<p align='center' class='style1'>New Cashbox Balance: ".$finance->check_cashbox_balance()."</p>";
		if ($users->update_money_owed($amount, $userid)){
			echo "<p align='center' class='style1'>You are currently owed: ".$users->get_user("money_owed",$userid)."</p>";
		}
	}
}
else if($account=="Bank of Baroda account"){
	#update account
	$total=$finance->check_bank_baroda_balance()+$amount;
	$type="Addition";
	if ($finance->update_bank_baroda($total, $amount, $type)){
		echo "<p align='center' class='style1'>New Baroda Account Balance: ".$finance->check_bank_baroda_balance()."</p>";
		if ($users->update_money_owed($amount, $userid)){
			echo "<p align='center' class='style1'>You are currently owed: ".$users->get_user("money_owed",$userid)."</p>";
		}
	}
}
else if($account=="HFCK Bank account"){
	#update account
	$total=$finance->check_bank_hfck_balance()+$amount;
	$type="Addition";
	if ($finance->update_bank_hfck($total, $amount, $type)){
		echo "<p align='center' class='style1'>New HFCK Account Balance: ".$finance->check_bank_hfck_balance()."</p>";
		if ($users->update_money_owed($amount, $userid)){
			echo "<p align='center' class='style1'>You are currently owed: ".$users->get_user("money_owed",$userid)."</p>";
		}
	}
}
else if($option=="Rafiki DTM account"){
	#update account
	$total=$finance->check_bank_rafiki_balance()+$amount;
	$type="Addition";
	if ($finance->update_bank_rafiki($total, $amount, $type)){
		echo "<p align='center' class='style1'>New Balance: ".$finance->check_bank_rafiki_balance()."</p>";
		if ($users->update_money_owed($amount, $userid)){
			echo "<p align='center' class='style1'>You are currently owed: ".$users->get_user("money_owed",$userid)."</p>";
		}
	}
}

?>
  <p align="center">&nbsp;</p>

<p align="center">&nbsp;</p>
</div>
</body>
</html>
