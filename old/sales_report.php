<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Finance System</title>
<link rel="stylesheet" href="style.css" type="text/css" />
</head>

<body>

<div class="content">
<p align="center">
<?php
include "finance_config.php";
$query="SELECT * FROM jobs ORDER BY date DESC";
$result=mysql_query($query);
echo "<p align='center' class='style1'>SALES REPORT</p>";
echo "<table width='700' border='1' cellspacing='2' cellpadding='0' align='center'>";
  echo "<tr>";
    echo "<th width='16%' scope='row' align='center'><b>Date</b></th>";
    echo "<td width='16%' align='center'><b>Job Name</b></td>";
    echo "<td width='16%' align='center'><b>Amount</b></td>";
    echo "<td width='16%' align='center'><b>Amount Paid</b></td>";
    echo "<td width='16%' align='center'><b>Amount owed</b></td>";
  echo "</tr>";
echo "</table>";

while ($sales=mysql_fetch_object($result)){
echo "<table width='700' border='1' cellspacing='2' cellpadding='0' align='center'>";
  echo "<tr>";
    echo "<th width='16%' scope='row' align='center'>".$sales->date."</th>";
    echo "<td width='16%' align='center'>".$sales->job_name."</td>";
    echo "<td width='16%' align='center'>".$sales->total."</td>";
    echo "<td width='16%' align='center'>".$sales->amount_paid."</td>";
	$amount_owed=$sales->total-$sales->amount_paid;
    echo "<td width='16%' align='center'>".$amount_owed."</td>";
  echo "</tr>";
echo "</table>";
}
$query="SELECT SUM(total) AS total FROM jobs";
$result=mysql_query($query);
while ($total=mysql_fetch_object($result)){
$salestotal=$total->total;
}
$query="SELECT SUM(amount_paid) AS paid FROM jobs";
$result=mysql_query($query);
while ($total=mysql_fetch_object($result)){
$paid=$total->paid;
}
$owed=$salestotal-$paid;
echo "<table width='500' border='1' cellspacing='2' cellpadding='0' align='center'>";
  echo "<tr>";
    echo "<th width='210' scope='row' align='center'><b>TOTALS</b></th>";
	echo "<td width='90' align='center'><b>".$salestotal."</b></td>";
    echo "<td width='100' align='center'><b>".$paid."</b></td>";
    echo "<td width='100' align='center'><b>".$owed."</b></td>";
  echo "</tr>";
echo "</table>";

?>
</p>
<p align="center"><a class="button" href="sales_report_print.php">Print</a><a class="button" href="reports.php">Back</a></p>
</div>
</body>
</html>