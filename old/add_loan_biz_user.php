<?php
$id=$_COOKIE['id'];
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link href="style.css" rel="stylesheet" type="text/css"  />
<title>Finance System</title>
</head>
<body>
<div class="content">
<?php
#set all the varibles from the form
include "finance_config.php";
include "lib/finance.php";
include "lib/users.php";

$finance = new finance;
$users = new users;
$user=$_POST['user'];
$account=$_POST['account'];
$amount=$_POST['amount'];
$date=date("d-m-Y");
$userid=$_COOKIE['id'];
$details="Loan issued to ".$users->get_user('user_name',$user);
$type="Loan deduction";

#if a cashbox transaction
if ($account=="Cashbox"){
	#Check if balance is enough
	if ($amount>$finance->check_cashbox_balance()){
	echo "<p align='center' class='style1'>The cashbox does not have enough money to handle this transaction</p>";
	}
	else{
		#record the loan in the db
		if ($users->add_loan_user($user,$amount)){
			#update cashbox
			$total=$finance->check_cashbox_balance()-$amount;
			if ($finance->update_cashbox($total, $amount, $type)){
				echo "New Balance: ".$finance->check_cashbox_balance();
				$transaction_id="cb".$finance->check_cashbox_id();
				#set the expense as administrative
				if ($finance->enter_admin_expense($amount, $type, 0, $transaction_id, $details)){
					echo "<p align='center' class='style1'>Loan credited and Admin expense entered. We good</p>";
				}
			}
		}
		else{
			echo "Something went wrong";
		}
	}
}
else if($account=="Bank of Baroda account"){
	#Check if balance is enough
	if ($amount>$finance->check_bank_baroda_balance()){
	echo "<p align='center' class='style1'>The account does not have enough money to handle this transaction</p>";
	}
	else{
		#record the loan in the db
		if ($users->add_loan_user($user,$amount)){
			#update account
			$total=$finance->check_bank_baroda_balance()-$amount;
			if ($finance->update_bank_baroda($total, $amount, $type)){
				echo "New Balance: ".$finance->check_bank_baroda_balance();
				$transaction_id="cb".$finance->check_bank_baroda_id();
				#set the expense as administrative
				if ($finance->enter_admin_expense($amount, $type, 0, $transaction_id, $details)){
					echo "<p align='center' class='style1'>Loan credited and Admin expense entered. We good</p>";
				}
			}
		}
		else{
			echo "Something went wrong";
		}
	}
}
else if($account=="HFCK Bank account"){
	#Check if balance is enough
	if ($amount>$finance->check_bank_hfck_balance()){
	echo "<p align='center' class='style1'>The account does not have enough money to handle this transaction</p>";
	}
	else{
		#record the loan in the db
		if ($users->add_loan_user($user,$amount)){
			#update account
			$total=$finance->check_bank_hfck_balance()-$amount;
			if ($finance->update_bank_hfck($total, $amount, $type)){
				echo "New Balance: ".$finance->check_bank_hfck_balance();
				$transaction_id="cb".$finance->check_bank_hfck_id();
				#set the expense as administrative
				if ($finance->enter_admin_expense($amount, $type, 0, $transaction_id, $details)){
					echo "<p align='center' class='style1'>Loan credited and Admin expense entered. We good</p>";
				}
			}
		}
		else{
			echo "Something went wrong";
		}
	}
}

else if($account=="Rafiki DTM account"){
	#Check if balance is enough
	if ($amount>$finance->check_bank_rafiki_balance()){
	echo "<p align='center' class='style1'>The account does not have enough money to handle this transaction</p>";
	}
	else{
		#record the loan in the db
		if ($users->add_loan_user($user,$amount)){
			#update account
			$total=$finance->check_bank_rafiki_balance()-$amount;
			if ($finance->update_bank_rafiki($total, $amount, $type)){
				echo "New Balance: ".$finance->check_bank_rafiki_balance();
				$transaction_id="cb".$finance->check_bank_rafiki_id();
				#set the expense as administrative
				if ($finance->enter_admin_expense($amount, $type, 0, $transaction_id, $details)){
					echo "<p align='center' class='style1'>Loan credited and Admin expense entered. We good</p>";
				}
			}
		}
		else{
			echo "Something went wrong";
		}
	}
}
?>
  <p align="center">&nbsp;</p>

<p align="center">&nbsp;</p>
</div>
</body>
</html>
