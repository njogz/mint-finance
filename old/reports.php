<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link rel="stylesheet" href="style.css" type="text/css" />
<title>Finance System</title>

</head>

<body>

<div class="content">
<p align="center" class="style1">REPORTS</p>
<p align="center"><a class="button" href="opexpenses_report.php">OPERATING EXPENSES REPORT</a></p>
<p align="center"><a class="button" href="adminexpenses_report.php">ADMIN EXPENSES REPORT</a></p>
<p align="center"><a class="button" href="reports/sales_report.php">TOTAL SALES REPORT</a></p>
<p align="center"><a class="button" href="reports/cashbox_report.php">CASHBOX REPORT</a></p>
<p align="center"><a class="button" href="reports/baroda_report.php">BANK OF BARODA REPORT</a></p>
<p align="center"><a class="button" href="reports/hfck_report.php">HFCK BANK REPORT</a></p>
<p align="center"><a class="button" href="reports/rafiki_report.php">RAFIKI DTM REPORT</a></p>
<p align="center"><a class="button" href="index.php">Back</a></p>
</div>
</body>
</html>
