<?php
include "finance_config.php";
class jobs{
	public function add_job_card($name,$client,$comments){
		$status="incomplete";
		$date=date("d-m-Y");
		$query = "INSERT INTO job_card (name,status,client_id,date_created,comments) VALUES ('$name','$status','$client','$date','$comments')";
		$result = mysql_query($query);
		if ($result){
			return TRUE;
		}
		else{
			return FALSE;
		}
	}
	
	public function add_job($card_id,$particulars,$quantity,$amount){
		$query = "INSERT INTO job (card_id,particulars,quantity,amount) VALUES ('$card_id','$particulars','$quantity','$amount')";
		$result = mysql_query($query);
		if ($result){
			return TRUE;
		}
		else{
			return FALSE;
		}
	}
	
	public function create_invoice($client_id,$card_id){
		$query = "INSERT INTO invoices (client_id,job_id,status) values ('$client_id','$card_id','unpaid')";
		$result = mysql_query($query);
		if ($result){
			$this->update_card_invoice($card_id);
			return TRUE;
		}
		else{
			return FALSE;
		}
	}
	
	public function update_card_invoice($card_id){
		$query = "SELECT invoice_id from invoices WHERE job_id=$card_id";
		$result = mysql_query($query);
		while ($job=mysql_fetch_row($result)){
			$field=$job['0'];
		}
		$invoice=$field;
		$query1 = "UPDATE job_card SET invoice_id=$invoice WHERE card_id=$card_id";
		$result1 = mysql_query($query1);
		if ($result1){
			return TRUE;
		}
		else{
			return FALSE;
		}
	}
	
	public function get_card_number($client){
		$query = "SELECT card_id from  job_card WHERE client_id=$client ORDER BY card_id ASC";
		$result = mysql_query($query);
		while ($user=mysql_fetch_row($result)){
			$field=$user['0'];
		}
		return $field;
	}
	
	public function get_card_status($card_id){
		$query = "SELECT status from  job_card WHERE card_id=$card_id";
		$result = mysql_query($query);
		while ($status=mysql_fetch_row($result)){
			$field=$status['0'];
		}
		return $field;
	}
	
	public function get_card_field($field, $card_id){
		$query = "SELECT $field from  job_card WHERE card_id=$card_id";
		$result = mysql_query($query);
		while ($user=mysql_fetch_row($result)){
			$field=$user['0'];
		}
		return $field;
	}
	
	public function get_client_details($field, $client_id){
		$query = "SELECT $field from  clients WHERE client_id=$client_id";
		$result = mysql_query($query);
		while ($user=mysql_fetch_row($result)){
			$field=$user['0'];
		}
		return $field;
	}
	
	public function get_job_field($field, $job_id){
		$query = "SELECT $field from  job WHERE job_id=$job_id";
		$result = mysql_query($query);
		while ($user=mysql_fetch_row($result)){
			$field=$user['0'];
		}
		return $field;
	}
	
	public function get_change_field($field, $change_id){
		$query = "SELECT $field from  pending_changes WHERE change_id=$change_id";
		$result = mysql_query($query);
		while ($user=mysql_fetch_row($result)){
			$field=$user['0'];
		}
		return $field;
	}
	
	public function get_card_expenses($card_id){
		$query = "SELECT SUM(amount) FROM expenses WHERE job_id=$card_id";
		$result = mysql_query($query);
		while ($user=mysql_fetch_row($result)){
			$field=$user['0'];
		}
		return $field;
	}
	
	public function get_all_jobs($card_id){
		$query = "SELECT * FROM job WHERE card_id=$card_id";
		$result = mysql_query($query);
		$counter = 1;
		while ($job=mysql_fetch_object($result)){
			$jobs .='<tr><td style="width:8%;">'.$counter.'</td>
			<td style="text-align:left; padding-left:10px;">'.$job->particulars.'</td>
			<td class="mono" style="width:15%;">'.$job->quantity.'</td>
			<td style="width:15%;" class="mono">'.$job->amount.'</td>
			<td style="width:15%;" class="mono">'.$job->quantity*$job->amount.'</td></tr>';
			$counter++;
		}
		return $jobs;
	}
	
	public function update_card_total($card,$total){
		$query = "SELECT total from job_card WHERE card_id=$card";
		$result = mysql_query($query);
		while ($job=mysql_fetch_row($result)){
			$field=$job['0'];
		}
		$new_total=$field+$total;
		$query1 = "UPDATE job_card SET total=$new_total WHERE card_id=$card";
		$result1 = mysql_query($query1);
		if ($result1){
			return TRUE;
		}
		else{
			return FALSE;
		}
	}
	
	public function update_card_paid($card,$total){
		$query = "SELECT amount_paid from job_card WHERE card_id=$card";
		$result = mysql_query($query);
		while ($job=mysql_fetch_row($result)){
			$field=$job['0'];
		}
		$new_total=$field+$total;
		$query1 = "UPDATE job_card SET amount_paid=$new_total WHERE card_id=$card";
		$result1 = mysql_query($query1);
		if ($result1){
			return TRUE;
		}
		else{
			return FALSE;
		}
	}
	
	public function update_invoice_amount($card,$total){
		$query = "SELECT amount from invoices WHERE job_id=$card";
		$result = mysql_query($query);
		while ($job=mysql_fetch_row($result)){
			$field=$job['0'];
		}
		$new_total=$field+$total;
		$query1 = "UPDATE invoices SET amount=$new_total WHERE job_id=$card";
		$result1 = mysql_query($query1);
		if ($result1){
			return TRUE;
		}
		else{
			return FALSE;
		}
	}
	
	public function update_client_jobs($client){
		$query = "SELECT jobs from clients WHERE client_id=$client";
		$result = mysql_query($query);
		while ($job=mysql_fetch_row($result)){
			$field=$job['0'];
		}
		$jobs=$field+1;
		$query1 = "UPDATE clients SET jobs=$jobs WHERE client_id=$client";
		$result1 = mysql_query($query1);
		if ($result1){
			return TRUE;
		}
		else{
			return FALSE;
		}
	}
}
?>
