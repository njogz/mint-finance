<?php
include "finance_config.php";
class finance{
	public function check_cashbox_balance(){
		$query="SELECT total FROM cashbox ORDER BY transaction_id ASC";
		$result=mysql_query($query);
		while ($cashier=mysql_fetch_row($result)){
			$field=$cashier['0'];
		}
		return $field;
	}
	
	public function check_bank_baroda_balance(){
		$query="SELECT total FROM bank_baroda ORDER BY transaction_id ASC";
		$result=mysql_query($query);
		while ($cashier=mysql_fetch_row($result)){
			$field=$cashier['0'];
		}
		return $field;
	}
	
	public function check_bank_hfck_balance(){
		$query="SELECT total FROM bank_hfck ORDER BY transaction_id ASC";
		$result=mysql_query($query);
		while ($cashier=mysql_fetch_row($result)){
			$field=$cashier['0'];
		}
		return $field;
	}
	
	public function check_bank_rafiki_balance(){
		$query="SELECT total FROM bank_rafiki ORDER BY transaction_id ASC";
		$result=mysql_query($query);
		while ($cashier=mysql_fetch_row($result)){
			$field=$cashier['0'];
		}
		return $field;
	}
	
	public function check_cashbox_id(){
		$query="SELECT transaction_id FROM cashbox ORDER BY transaction_id ASC";
		$result=mysql_query($query);
		while ($cashier=mysql_fetch_row($result)){
			$field=$cashier['0'];
		}
		return $field;
	}
	
	public function check_bank_baroda_id(){
		$query="SELECT transaction_id FROM bank_baroda ORDER BY transaction_id ASC";
		$result=mysql_query($query);
		while ($cashier=mysql_fetch_row($result)){
			$field=$cashier['0'];
		}
		return $field;
	}
	
	public function check_bank_hfck_id(){
		$query="SELECT transaction_id FROM bank_hfck ORDER BY transaction_id ASC";
		$result=mysql_query($query);
		while ($cashier=mysql_fetch_row($result)){
			$field=$cashier['0'];
		}
		return $field;
	}
	
	public function check_bank_rafiki_id(){
		$query="SELECT transaction_id FROM bank_rafiki ORDER BY transaction_id ASC";
		$result=mysql_query($query);
		while ($cashier=mysql_fetch_row($result)){
			$field=$cashier['0'];
		}
		return $field;
	}
	
	public function update_cashbox($total, $amount, $type){
		$date=date('d-m-Y');
		$userid=$_COOKIE['id'];
		$query = "INSERT INTO cashbox (cash,total,date,user_id,type) VALUES ('$amount','$total','$date','$userid','$type')";
		$result = mysql_query($query);
		if ($result){
			return TRUE;
		}
		else{
			return FALSE;
		}
	}
	public function pay_payable($amount,$id){
		$amount_due=($this->get_payable_field("amount_due",$id))-$amount;
		$query="UPDATE payables SET amount_due=$amount_due WHERE payable_id=$id";
		$result=mysql_query($query);
		if ($result){
			return TRUE;
		}
		else{
			return FALSE;
		}
		
	}
	public function get_payable_field($field,$id){
		$query="SELECT $field FROM payables WHERE payable_id=$id";
		$result=mysql_query($query);
		while ($cashier=mysql_fetch_row($result)){
			$field=$cashier['0'];
		}
		return $field;
	}
	
	public function update_payables($amount, $details){
		$date=date('d-m-Y');
		$userid=$_COOKIE['id'];
		$query = "INSERT INTO payables (amount,date_entered,details,amount_due) VALUES ('$amount','$date','$details','$amount')";
		$result = mysql_query($query);
		if ($result){
			return TRUE;
		}
		else{
			return FALSE;
		}
	}
	
	public function update_bank_baroda($total, $amount, $type){
		$date=date('d-m-Y');
		$userid=$_COOKIE['id'];
		$query = "INSERT INTO bank_baroda (cash,total,date,user_id,type) VALUES ('$amount','$total','$date','$userid','$type')";
		$result = mysql_query($query);
		if ($result){
			return TRUE;
		}
		else{
			return FALSE;
		}
	}
	
	public function update_bank_hfck($total, $amount, $type){
		$date=date('d-m-Y');
		$userid=$_COOKIE['id'];
		$query = "INSERT INTO bank_hfck (cash,total,date,user_id,type) VALUES ('$amount','$total','$date','$userid','$type')";
		$result = mysql_query($query);
		if ($result){
			return TRUE;
		}
		else{
			return FALSE;
		}
	}
	
	public function update_bank_rafiki($total, $amount, $type){
		$date=date('d-m-Y');
		$userid=$_COOKIE['id'];
		$query = "INSERT INTO bank_rafiki (cash,total,date,user_id,type) VALUES ('$amount','$total','$date','$userid','$type')";
		$result = mysql_query($query);
		if ($result){
			return TRUE;
		}
		else{
			return FALSE;
		}
	}
	
	public function enter_loan($amount, $date, $interest){
		$amount_due=($amount*($interest/100))+$amount;
		$query = "INSERT INTO loan (amount,due_date,interest,amount_due) VALUES ('$amount','$date','$interest','$amount_due')";
		$result = mysql_query($query);
		if ($result){
			return TRUE;
		}
		else{
			return FALSE;
		}
	}
	public function get_loan_field($field,$id){
		$query="SELECT $field FROM loan WHERE loan_id=$id";
		$result=mysql_query($query);
		while ($cashier=mysql_fetch_row($result)){
			$field=$cashier['0'];
		}
		return $field;
	}
	
	public function enter_admin_expense($amount, $type, $id, $transaction_id, $details){
		$date=date('d-m-Y');
		$userid=$_COOKIE['id'];
		$query = "INSERT INTO expenses (amount,type,job_id,transaction_id,user_id,category,date,details) VALUES ('$amount','$type','$id','$transaction_id','$userid','admin_expense','$date','$details')";
		$result = mysql_query($query);
		if ($result){
			return TRUE;
		}
		else{
			return FALSE;
		}
	}
	
	public function enter_operation_expense($amount, $type, $id, $transaction_id, $details){
		$date=date('d-m-Y');
		$userid=$_COOKIE['id'];
		$query = "INSERT INTO expenses (amount,type,job_id,transaction_id,user_id,category,date,details) VALUES ('$amount','$type','$id','$transaction_id','$userid','job_expense','$date','$details')";
		$result = mysql_query($query);
		if ($result){
			return TRUE;
		}
		else{
			return FALSE;
		}
	}
	
	public function enter_payment($amount, $id, $transaction_id){
		$date=date('d-m-Y');
		$query = "INSERT INTO payments (transaction_id,job_id,amount,date) VALUES ('$transaction_id','$id','$amount','$date')";
		$result = mysql_query($query);
		if ($result){
			return TRUE;
		}
		else{
			return FALSE;
		}
	}
	
	public function pay_biz_loan($amount,$id){
		$amount_due=($this->get_loan_field("amount_due",$id))-$amount;
		$query1 = "UPDATE loan SET amount_due=$amount_due WHERE loan_id=$id";
		$result1 = mysql_query($query1);
		if ($result1){
			return TRUE;
		}
		else{
			return FALSE;
		}
	}
	
	public function enter_payment1($amount, $id, $transaction_id, $details){
		$date=date('d-m-Y');
		$query = "INSERT INTO payments (transaction_id,job_id,amount,date,details) VALUES ('$transaction_id','$id','$amount','$date','$details')";
		$result = mysql_query($query);
		if ($result){
			return TRUE;
		}
		else{
			return FALSE;
		}
	}
}
?>
