<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link rel="stylesheet" href="style.css" type="text/css" />
<title>Finance System</title>
<style type="text/css">
<!--
.style1 {
	color: #0066CC;
	font-size: 24px;
	font-weight: bold;
}
-->
</style>
</head>

<body>

<div class="content">
<p align="center" class="style1">ADD A NEW JOB</p>
<?php
#set all variables from the form
	include "finance_config.php";
	include "lib/jobs.php";
	$jobs = new jobs;
	$name=$_POST['job_name'];
	$client=$_POST['client'];
	$comments=$_POST['comments'];
#add new job card for the client
if($jobs->add_job_card($name,$client,$comments)){
	#create an invoice for the job card
	$jobs->create_invoice($client,$jobs->get_card_number($client));
	
	#update client job count
	$jobs->update_client_jobs($client);
?>
	<form id="form1" name="form1" method="post" action="addjob.php">
<input name="job_card" type="hidden" value="<?php echo $jobs->get_card_number($client); ?>"/>
<table width="661" border="0"  align="center" cellpadding="0" cellspacing="10">
  <tr>
    <th width="166" align="left" scope="row"><label><span class="style4">Job Name</span></label></th>
    <td width="381"><input type="text" name="job_name" id="job_name" /></td>
  </tr>
  <tr>
    <th align="left" scope="row">Particulars</th>
    <td><label>
      <textarea name="particulars" id="particulars" cols="45" rows="5"></textarea>
    </label></td>
  </tr>
  <tr>
    <th width="166" align="left" scope="row"><label><span class="style4">Quantity</span></label></th>
    <td width="381"><input type="text" name="quantity" id="quantity" /></td>
  </tr>
  <tr>
    <th width="166" align="left" scope="row"><label><span class="style4">Amount</span></label></th>
    <td width="381"><input type="text" name="amount" id="amount" /></td>
  </tr>
  <tr>
    <th scope="row">&nbsp;</th>
    <td><label>
      <input type="submit" name="submit" id="submit" value="Add job" />
    </label></td>
  </tr>
</table>
</form>
<?php
}
else{
	echo "<p style='color:#FFFFFF'>Something went wrong</p>";
}
	?>
<p align="center"><a class="button" href="view_client.php">Back</a><a class="homepage" href="index.php">Back to Homepage</a></p>
<p align="center">&nbsp;</p>
</div>
</body>
</html>
