<?php
if (!isset($_COOKIE['id'])) {
	header("location:../index.php");
}
include "../users/users.php";
include "clients.php";

$user = new users;
$client = new clients;
?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <title>Mintgraphics Finance</title>
    <meta content="IE=edge,chrome=1" http-equiv="X-UA-Compatible">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="Njuguna Ndung'u">

    <link rel="stylesheet" type="text/css" href="../lib/bootstrap/css/bootstrap.css">
    
    <link rel="stylesheet" type="text/css" href="../stylesheets/theme.css">
    <link rel="stylesheet" href="../lib/font-awesome/css/font-awesome.css">

    <script src="../lib/jquery-1.7.2.min.js" type="text/javascript"></script>

    <!-- Demo page code -->

    <style type="text/css">
        #line-chart {
            height:300px;
            width:800px;
            margin: 0px auto;
            margin-top: 1em;
        }
        .brand { font-family: georgia, serif; }
        .brand .first {
            color: #ccc;
            font-style: italic;
        }
        .brand .second {
            color: #fff;
            font-weight: bold;
        }
    </style>

    <!-- Le HTML5 shim, for IE6-8 support of HTML5 elements -->
    <!--[if lt IE 9]>
      <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->

    <!-- Le fav and touch icons -->
    <link rel="shortcut icon" href="../assets/ico/favicon.ico">
    <link rel="apple-touch-icon-precomposed" sizes="144x144" href="../../assets/ico/apple-touch-icon-144-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="114x114" href="../../assets/ico/apple-touch-icon-114-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="72x72" href="../../assets/ico/apple-touch-icon-72-precomposed.png">
    <link rel="apple-touch-icon-precomposed" href="../../assets/ico/apple-touch-icon-57-precomposed.png">
  </head>

  <!--[if lt IE 7 ]> <body class="ie ie6"> <![endif]-->
  <!--[if IE 7 ]> <body class="ie ie7 "> <![endif]-->
  <!--[if IE 8 ]> <body class="ie ie8 "> <![endif]-->
  <!--[if IE 9 ]> <body class="ie ie9 "> <![endif]-->
  <!--[if (gt IE 9)|!(IE)]><!--> 
  <body class=""> 
  <!--<![endif]-->
    
    <div class="navbar">
        <div class="navbar-inner">
                <ul class="nav pull-right">
                    
                    <!-- <li><a href="#" class="hidden-phone visible-tablet visible-desktop" role="button">Settings</a></li> -->
                    <li id="fat-menu" class="dropdown">
                        <a href="#" role="button" class="dropdown-toggle" data-toggle="dropdown">
                            <i class="icon-user"></i>
                            <?php
								$name = $user->get_user("user_name",$_COOKIE['id']);
								echo $name;                            
                            ?>
                            <i class="icon-caret-down"></i>
                        </a>

                        <ul class="dropdown-menu">
							<li><a tabindex="-1" href="#">Notifications</a></li>
                            <li class="divider"></li>
                            <li><a tabindex="-1" href="#">My Account</a></li>
                            <li class="divider"></li>
                            <!-- <li><a tabindex="-1" class="visible-phone" href="#">Settings</a></li> -->
                            <li class="divider visible-phone"></li>
                            <li><a tabindex="-1" href="users/logout.php">Logout</a></li>
                        </ul>
                    </li>
                    
                </ul>
                <a class="brand" href="index.html"><span class="first">Mint</span> <span class="second">Graphics</span></a>
        </div>
    </div>
    <div class="sidebar-nav">
        <form class="search form-inline">
            <input type="text" placeholder="Search...">
        </form>

        <?php
			define('ROOT', getcwd());
			require_once ROOT . '../../menu/sidemenu.php';
		?>
    </div>
    

    
    <div class="content">
        
        <div class="header">
            
            <h1 class="page-title">View Clients</h1>
        </div>
        
                <ul class="breadcrumb">
            <li><a href="../dashboard.php">Home</a> <span class="divider">/</span></li>
            <li class="active">Clients <span class="divider">/</span></li>
            <li class="active">View clients</li>
        </ul>

        <div class="container-fluid">
            <div class="row-fluid">
                    

<div class="row-fluid">
    <div class="block ">
		<table  class="table table-bordered">
			<thead>
				<tr><td>Name</td><td>Number</td><td>Company</td><td>No. of jobs</td><td>Client value</td><td>Amount outstanding</td><td>Edit</td></tr>
			</thead>
			<tbody>
				<?php
				$tbl_name="clients";		//your table name
				// How many adjacent pages should be shown on each side?
				$adjacents = 3;
				
				/* 
				   First get total number of rows in data table. 
				   If you have a WHERE clause in your query, make sure you mirror it here.
				*/
				$query = "SELECT COUNT(*) as num FROM $tbl_name";
				$total_pages = mysql_fetch_array(mysql_query($query));
				$total_pages = $total_pages[num];
				
				/* Setup vars for query. */
				$targetpage = "view_clients.php"; 	//your file name  (the name of this file)
				$limit = 50; 								//how many items to show per page
				$page = $_GET['page'];
				if($page) 
					$start = ($page - 1) * $limit; 			//first item to display on this page
				else
					$start = 0;								//if no page var is given, set start to 0
				
				/* Get data. */
				$sql = "SELECT * FROM $tbl_name ORDER BY first_name ASC LIMIT $start, $limit";
				$result = mysql_query($sql);
				
				/* Setup page vars for display. */
				if ($page == 0) $page = 1;					//if no page var is given, default to 1.
				$prev = $page - 1;							//previous page is page - 1
				$next = $page + 1;							//next page is page + 1
				$lastpage = ceil($total_pages/$limit);		//lastpage is = total pages / items per page, rounded up.
				$lpm1 = $lastpage - 1;						//last page minus 1
				
				/* 
					Now we apply our rules and draw the pagination object. 
					We're actually saving the code to a variable in case we want to draw it more than once.
				*/
				$pagination = "";
				if($lastpage > 1)
				{	
					$pagination .= "<div class=\"pagination\">";
					//previous button
					if ($page > 1) 
						$pagination.= "<a href=\"$targetpage?page=$prev\"><< previous</a>";
					else
						$pagination.= "<span class=\"disabled\"><< previous</span>";	
					
					//pages	
					if ($lastpage < 7 + ($adjacents * 2))	//not enough pages to bother breaking it up
					{	
						for ($counter = 1; $counter <= $lastpage; $counter++)
						{
							if ($counter == $page)
								$pagination.= "<span class=\"current\">$counter</span>";
							else
								$pagination.= "<a href=\"$targetpage?page=$counter\">$counter</a>";					
						}
					}
					elseif($lastpage > 5 + ($adjacents * 2))	//enough pages to hide some
					{
						//close to beginning; only hide later pages
						if($page < 1 + ($adjacents * 2))		
						{
							for ($counter = 1; $counter < 4 + ($adjacents * 2); $counter++)
							{
								if ($counter == $page)
									$pagination.= "<span class=\"current\">$counter</span>";
								else
									$pagination.= "<a href=\"$targetpage?page=$counter\">$counter</a>";					
							}
							$pagination.= "...";
							$pagination.= "<a href=\"$targetpage?page=$lpm1\">$lpm1</a>";
							$pagination.= "<a href=\"$targetpage?page=$lastpage\">$lastpage</a>";		
						}
						//in middle; hide some front and some back
						elseif($lastpage - ($adjacents * 2) > $page && $page > ($adjacents * 2))
						{
							$pagination.= "<a href=\"$targetpage?page=1\">1</a>";
							$pagination.= "<a href=\"$targetpage?page=2\">2</a>";
							$pagination.= "...";
							for ($counter = $page - $adjacents; $counter <= $page + $adjacents; $counter++)
							{
								if ($counter == $page)
									$pagination.= "<span class=\"current\">$counter</span>";
								else
									$pagination.= "<a href=\"$targetpage?page=$counter\">$counter</a>";					
							}
							$pagination.= "...";
							$pagination.= "<a href=\"$targetpage?page=$lpm1\">$lpm1</a>";
							$pagination.= "<a href=\"$targetpage?page=$lastpage\">$lastpage</a>";		
						}
						//close to end; only hide early pages
						else
						{
							$pagination.= "<a href=\"$targetpage?page=1\">1</a>";
							$pagination.= "<a href=\"$targetpage?page=2\">2</a>";
							$pagination.= "...";
							for ($counter = $lastpage - (2 + ($adjacents * 2)); $counter <= $lastpage; $counter++)
							{
								if ($counter == $page)
									$pagination.= "<span class=\"current\">$counter</span>";
								else
									$pagination.= "<a href=\"$targetpage?page=$counter\">$counter</a>";					
							}
						}
					}
					
					//next button
					if ($page < $counter - 1) 
						$pagination.= "<a href=\"$targetpage?page=$next\">next >></a>";
					else
						$pagination.= "<span class=\"disabled\">next >></span>";
					$pagination.= "</div>\n";		
				}

					while($clients = mysql_fetch_object($result))
					{
						 echo "<tr>";
							echo "<td><a href='client.php?id=$clients->client_id'>".$clients->first_name." ".$clients->last_name."</a></td>";
							echo "<td>".$clients->client_number."</td>";
							echo "<td>".$clients->company."</td>";
							echo "<td>".$clients->jobs."</td>";
							echo "<td>".number_format($client->client_value($clients->client_id),2)."</td>";
							$balance=$client->client_value($clients->client_id)-$client->client_amount_paid($clients->client_id);
							echo "<td>".number_format($balance,2)."</td>";
							echo "<td><a href='edit_client.php?id=$clients->client_id'>Edit</td>";
						  echo "</tr>";
					}
			?>
			</tbody>
		</table>
		<?=$pagination?>
    </div>
</div>

                    
                    <footer>
                        <hr>
                        
                        <p>&copy; <?php echo date('Y')?> Mint Graphics</p>
                    </footer>
                    
            </div>
        </div>
    </div>
    


    <script src="../lib/bootstrap/js/bootstrap.js"></script>
    <script type="text/javascript">
        $("[rel=tooltip]").tooltip();
        $(function() {
            $('.demo-cancel-click').click(function(){return false;});
        });
    </script>
    
  </body>
</html>
