<?php
#set all the varibles
include "../users/users.php";
include "finance.php";
include "../finance_config.php";

$finance = new finance;
$users = new users;

$option=$_POST['account'];
$amount=$_POST['amount'];
$type="Payable payment";
$id=$_POST['payableid'];;
$userid=$_COOKIE['id'];

#if account not selected
if ($option=="Select one"){
	echo "<META HTTP-EQUIV=\"Refresh\" CONTENT=\"1;	URL=pay_payables.php?id=$id&m=error\">";	
}

#if a cashbox transaction
if ($option=="Cashbox"){
	#Check if balance is enough
	if ($amount>$finance->check_cashbox_balance()){
		echo "<META HTTP-EQUIV=\"Refresh\" CONTENT=\"1;	URL=pay_payables.php?id=$id&m=not_enough&account=cashbox\">";
	}
	else{
		#update cashbox
		$total=$finance->check_cashbox_balance()-$amount;
		if ($finance->update_cashbox($total, $amount, $type, 'deduction')){
			$balance=$finance->check_cashbox_balance();
			$transaction_id="cb".$finance->check_cashbox_id();
			#update the payable
			if($finance->pay_payable($amount,$id)){
				echo "<META HTTP-EQUIV=\"Refresh\" CONTENT=\"1;	URL=payables.php?m=success&balance=$balance\">";
			}
		}
	}
}
else if($option=="Bank of Baroda account"){
	#Check if balance is enough
	if ($amount>$finance->check_bank_baroda_balance()){
		echo "<META HTTP-EQUIV=\"Refresh\" CONTENT=\"1;	URL=pay_payables.php?id=$id&m=not_enough&account=baroda\">";
	}
	else{
		#update account
		$total=$finance->check_bank_baroda_balance()-$amount;
		//$type="Deduction";
		if ($finance->update_bank_baroda($total, $amount, $type)){
			$balance=$finance->check_bank_baroda_balance();
			$transaction_id="bb".$finance->check_bank_baroda_id();
			#update the payable
			if($finance->pay_payable($amount,$id)){
				echo "<META HTTP-EQUIV=\"Refresh\" CONTENT=\"1;	URL=payables.php?m=success&balance=$balance\">";
			}
		}
	}
}
else if($option=="HFCK Bank account"){
	#Check if balance is enough
	if ($amount>$finance->check_bank_hfck_balance()){
		echo "<META HTTP-EQUIV=\"Refresh\" CONTENT=\"1;	URL=pay_payables.php?id=$id&m=not_enough&account=HFCK\">";;
	}
	else{
		#update account
		$total=$finance->check_bank_hfck_balance()-$amount;
		//$type="Deduction";
		if ($finance->update_bank_hfck($total, $amount, $type)){
			$balance=$finance->check_bank_hfck_balance();
			$transaction_id="bh".$finance->check_bank_hfck_id();
			#update the payable
			if($finance->pay_payable($amount,$id)){
				echo "<META HTTP-EQUIV=\"Refresh\" CONTENT=\"1;	URL=payables.php?m=success&balance=$balance\">";
			}
		}
	}
}
else if($option=="Rafiki DTM account"){
	#Check if balance is enough
	if ($amount>$finance->check_bank_rafiki_balance()){
		echo "<META HTTP-EQUIV=\"Refresh\" CONTENT=\"1;	URL=pay_payables.php?id=$id&m=not_enough&account=rafiki\">";
	}
	else{
		#update account
		$total=$finance->check_bank_rafiki_balance()-$amount;
		//$type="Deduction";
		if ($finance->update_bank_rafiki($total, $amount, $type)){
			$balance=$finance->check_bank_rafiki_balance();
			$transaction_id="br".$finance->check_bank_rafiki_id();
			#update the payable
			if($finance->pay_payable($amount,$id)){
				echo "<META HTTP-EQUIV=\"Refresh\" CONTENT=\"1;	URL=payables.php?m=success&balance=$balance\">";
			}
		}
	}
}
else if($option=="Personal Money"){
	#update money owed to user
	if ($users->update_money_owed($amount, $userid)){
		$balance=$users->get_user("money_owed",$userid);
		$transaction_id="user".$userid;
		#update the payable
			if($finance->pay_payable($amount,$id)){
				echo "<META HTTP-EQUIV=\"Refresh\" CONTENT=\"1;	URL=payables.php?m=success&balance=$balance\">";
			}
	}	
}
?>
