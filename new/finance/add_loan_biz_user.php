<?php
#set all the varibles
include "../users/users.php";
include "finance.php";
include "../finance_config.php";

$finance = new finance;
$users = new users;

$user=$_POST['user'];
$account=$_POST['account'];
$amount=$_POST['amount'];
$date=date("d-m-Y");
$userid=$_COOKIE['id'];
$details="Loan issued to ".$users->get_user('user_name',$user);
$type="Loan deduction";

#if a cashbox transaction
if ($account=="Cashbox"){
	#Check if balance is enough
	if ($amount>$finance->check_cashbox_balance()){
		echo "<META HTTP-EQUIV=\"Refresh\" CONTENT=\"1; URL=loan_biz_user.php?m=not_enough&account=cashbox\">";
	}
	else{
		#record the loan in the db
		if ($users->add_loan_user($user,$amount)){
			#update cashbox
			$total=$finance->check_cashbox_balance()-$amount;
			if ($finance->update_cashbox($total, $amount, $type, 'deduction')){
				$balance=$finance->check_cashbox_balance();
				$transaction_id="cb".$finance->check_cashbox_id();
				#set the expense as administrative
				if ($finance->enter_admin_expense($amount, $type, 0, $transaction_id, $details)){
					echo "<META HTTP-EQUIV=\"Refresh\" CONTENT=\"1; URL=loan_biz_user.php?m=success&balance=$balance\">";
				}
			}
		}
		else{
			echo "Something went wrong";
		}
	}
}
else if($account=="Bank of Baroda account"){
	#Check if balance is enough
	if ($amount>$finance->check_bank_baroda_balance()){
		echo "<META HTTP-EQUIV=\"Refresh\" CONTENT=\"1; URL=loan_biz_user.php?m=not_enough&account=baroda\">";
	}
	else{
		#record the loan in the db
		if ($users->add_loan_user($user,$amount)){
			#update account
			$total=$finance->check_bank_baroda_balance()-$amount;
			if ($finance->update_bank_baroda($total, $amount, $type)){
				$balance=$finance->check_bank_baroda_balance();
				$transaction_id="cb".$finance->check_bank_baroda_id();
				#set the expense as administrative
				if ($finance->enter_admin_expense($amount, $type, 0, $transaction_id, $details)){
					echo "<META HTTP-EQUIV=\"Refresh\" CONTENT=\"1; URL=loan_biz_user.php?m=success&balance=$balance\">";
				}
			}
		}
		else{
			echo "Something went wrong";
		}
	}
}
else if($account=="HFCK Bank account"){
	#Check if balance is enough
	if ($amount>$finance->check_bank_hfck_balance()){
		echo "<META HTTP-EQUIV=\"Refresh\" CONTENT=\"1; URL=loan_biz_user.php?m=not_enough&account=hfck\">";
	}
	else{
		#record the loan in the db
		if ($users->add_loan_user($user,$amount)){
			#update account
			$total=$finance->check_bank_hfck_balance()-$amount;
			if ($finance->update_bank_hfck($total, $amount, $type)){
				$balance=$finance->check_bank_hfck_balance();
				$transaction_id="cb".$finance->check_bank_hfck_id();
				#set the expense as administrative
				if ($finance->enter_admin_expense($amount, $type, 0, $transaction_id, $details)){
					echo "<META HTTP-EQUIV=\"Refresh\" CONTENT=\"1; URL=loan_biz_user.php?m=success&balance=$balance\">";
				}
			}
		}
		else{
			echo "Something went wrong";
		}
	}
}

else if($account=="Rafiki DTM account"){
	#Check if balance is enough
	if ($amount>$finance->check_bank_rafiki_balance()){
		echo "<META HTTP-EQUIV=\"Refresh\" CONTENT=\"1; URL=loan_biz_user.php?m=not_enough&account=rafiki\">";
	}
	else{
		#record the loan in the db
		if ($users->add_loan_user($user,$amount)){
			#update account
			$total=$finance->check_bank_rafiki_balance()-$amount;
			if ($finance->update_bank_rafiki($total, $amount, $type)){
				$balance=$finance->check_bank_rafiki_balance();
				$transaction_id="cb".$finance->check_bank_rafiki_id();
				#set the expense as administrative
				if ($finance->enter_admin_expense($amount, $type, 0, $transaction_id, $details)){
					//echo "<p align='center' class='style1'>Loan credited and Admin expense entered. We good</p>";
					echo "<META HTTP-EQUIV=\"Refresh\" CONTENT=\"1; URL=loan_biz_user.php?m=success&balance=$balance\">";
				}
			}
		}
		else{
			echo "Something went wrong";
		}
	}
}
?>
