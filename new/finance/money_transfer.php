<?php
include "finance.php";
$finance = new finance;
$from=mysql_real_escape_string(trim($_POST['from']));
$to=mysql_real_escape_string(trim($_POST['to']));
$amount=mysql_real_escape_string(trim($_POST['amount']));

$amount=filter_var($amount, FILTER_SANITIZE_STRING);
$to=filter_var($to, FILTER_SANITIZE_STRING);
$from=filter_var($from, FILTER_SANITIZE_STRING);

$userid=$_COOKIE['id'];

if ($from==$to){
	echo "<META HTTP-EQUIV=\"Refresh\" CONTENT=\"1; URL=transfer.php?m=same\">";
}
else{
	if ($from==1){
		if ($finance->check_cashbox_balance()<$amount){
			echo "<META HTTP-EQUIV=\"Refresh\" CONTENT=\"1; URL=transfer.php?m=not_enough&account=cashbox\">";
		}
		else{
			$cashbox_total = $finance->check_cashbox_balance()-$amount;
			$cashbox_type = "Transfer to other account";
			$details="Deduction";
			if ($finance->update_cashbox($cashbox_total,$amount,$cashbox_type,$details)){
				if($to==2){
					$rafiki_total =  $finance->check_bank_rafiki_balance()+$amount;
					$rafiki_type = "Addition from cashbox";
					if ($finance->update_bank_rafiki($rafiki_total, $amount, $rafiki_type)){
						echo "<META HTTP-EQUIV=\"Refresh\" CONTENT=\"1; URL=transfer.php?m=success\">";
					}
				}
				elseif($to==3){
					$hfck_total = $finance->check_bank_hfck_balance()+$amount;
					$hfck_type = "Addition from cashbox";
					if ($finance->update_bank_hfck($hfck_total, $amount, $hfck_type)){
						echo "<META HTTP-EQUIV=\"Refresh\" CONTENT=\"1; URL=transfer.php?m=success\">";
					}
				}
				elseif($to==4){
					$baroda_total = $finance->check_bank_baroda_balance()+$amount;
					$baroda_type = "Addition from cashbox";
					if ($finance->update_bank_baroda($baroda_total, $amount, $baroda_type)){
						echo "<META HTTP-EQUIV=\"Refresh\" CONTENT=\"1; URL=transfer.php?m=success\">";
					}
				}
			}
		}
	}
	elseif ($from==2){
		if ($finance->check_bank_rafiki_balance()<$amount){
			echo "<META HTTP-EQUIV=\"Refresh\" CONTENT=\"1; URL=transfer.php?m=not_enough&account=rafiki\">";
		}
		else{
			$rafiki_total = $finance->check_bank_rafiki_balance()-$amount;
			$rafiki_type = "Deduction";
			if ($finance->update_bank_rafiki($rafiki_total,$amount,$rafiki_type)){
				if($to==3){
					$hfck_total = $finance->check_bank_hfck_balance()+$amount;
					$hfck_type = "Addition from rafiki";
					if ($finance->update_bank_hfck($hfck_total, $amount, $hfck_type)){
						echo "<META HTTP-EQUIV=\"Refresh\" CONTENT=\"1; URL=transfer.php?m=success\">";
					}
				}
				elseif($to==4){
					$baroda_total = $finance->check_bank_baroda_balance()+$amount;
					$baroda_type = "Addition from rafiki";
					if ($finance->update_bank_baroda($baroda_total, $amount, $baroda_type)){
						echo "<META HTTP-EQUIV=\"Refresh\" CONTENT=\"1; URL=transfer.php?m=success\">";
					}
				}
				elseif($to==1){
					$cashbox_total = $finance->check_cashbox_balance()+$amount;
					$cashbox_type="Addition from rafiki";
					if ($finance->update_cashbox($cashbox_total, $amount, $cashbox_type, 'Addition')){
						echo "<META HTTP-EQUIV=\"Refresh\" CONTENT=\"1; URL=transfer.php?m=success\">";
					}
				}
			}
		}
	}
	elseif ($from==3){
		if ($finance->check_bank_hfck_balance()<$amount){
			echo "<META HTTP-EQUIV=\"Refresh\" CONTENT=\"1; URL=transfer.php?m=not_enough&account=HFCK\">";
		}
		else{
			$hfck_total = $finance->check_bank_hfck_balance()-$amount;
			$hfck_type = "Deductuon";
			if ($finance->update_bank_hfck($hfck_total,$amount,$hfck_type)){
				if($to==4){
					$baroda_total = $finance->check_bank_baroda_balance()+$amount;
					$baroda_type = "Addition from hfck";
					if ($finance->update_bank_baroda($baroda_total, $amount, $baroda_type)){
						echo "<META HTTP-EQUIV=\"Refresh\" CONTENT=\"1; URL=transfer.php?m=success\">";
					}
				}
				elseif($to==1){
					$cashbox_total = $finance->check_cashbox_balance()+$amount;
					echo $cashbox_type="Addition from hfck";
					if ($finance->update_cashbox($cashbox_total, $amount, $cashbox_type, 'Addition')){
						echo "<META HTTP-EQUIV=\"Refresh\" CONTENT=\"1; URL=transfer.php?m=success\">";
					}
				}
				elseif($to==2){
					$rafiki_total =  $finance->check_bank_rafiki_balance()+$amount;
					$rafiki_type = "Addition from hfck";
					if ($finance->update_bank_rafiki($rafiki_total, $amount, $rafiki_type)){
						echo "<META HTTP-EQUIV=\"Refresh\" CONTENT=\"1; URL=transfer.php?m=success\">";
					}
				}
			}
		}
		
	}
	elseif ($from==4){
		if ($finance->check_bank_baroda_balance()<$amount){
			echo "<META HTTP-EQUIV=\"Refresh\" CONTENT=\"1; URL=transfer.php?m=not_enough&account=baroda\">";
		}
		else{
			$baroda_total = $finance->check_bank_baroda_balance()-$amount;
			$baroda_type = "Deduction";
			if ($finance->update_bank_baroda($baroda_total,$amount,$baroda_type)){
				if($to==1){
					$cashbox_total = $finance->check_cashbox_balance()+$amount;
					$cashbox_type="Addition from baroda";
					if ($finance->update_cashbox($cashbox_total, $amount, $cashbox_type, 'Addition')){
						echo "<META HTTP-EQUIV=\"Refresh\" CONTENT=\"1; URL=transfer.php?m=success\">";
					}
				}
				elseif($to==2){
					$rafiki_total =  $finance->check_bank_rafiki_balance()+$amount;
					$rafiki_type = "Addition from baroda";
					if ($finance->update_bank_rafiki($rafiki_total, $amount, $rafiki_type)){
						echo "<META HTTP-EQUIV=\"Refresh\" CONTENT=\"1; URL=transfer.php?m=success\">";
					}
				}
				elseif($to==3){
					$hfck_total = $finance->check_bank_hfck_balance()+$amount;
					$hfck_type = "Addition from baroda";
					if ($finance->update_bank_hfck($hfck_total, $amount, $hfck_type)){
						echo "<META HTTP-EQUIV=\"Refresh\" CONTENT=\"1; URL=transfer.php?m=success\">";
					}
				}
			}
		}
	}
	else{
		echo "<p align='center' class='style1'>An error occurred \n Please close this window and try again
		</p>";
	}
}

?>
