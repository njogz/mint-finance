<?php
#set all the varibles
include "../users/users.php";
include "finance.php";
include "../finance_config.php";

$finance = new finance;
$users = new users;
$account=$_POST['account'];
$amount=$_POST['amount'];
$date=$_POST['date'];
$interest=$_POST['interest'];

#if account not selected
if ($account=="Select one"){
	echo "<META HTTP-EQUIV=\"Refresh\" CONTENT=\"1;	URL=loan.php?m=error\">";	
}

#if a cashbox transaction
if ($account=="Cashbox"){
	#update cashbox
	$total=$finance->check_cashbox_balance()+$amount;
	$type="Business loan";
	if ($finance->update_cashbox($total, $amount, $type, 'addition')){
		$balance=$finance->check_cashbox_balance();
		if ($finance->enter_loan($amount, $date, $interest)){
			echo "<META HTTP-EQUIV=\"Refresh\" CONTENT=\"1;	URL=loan.php?m=success&balance=$balance\">";
		}
	}
}
else if($account=="Bank of Baroda account"){
	#update account
	$total=$finance->check_bank_baroda_balance()+$amount;
	$type="Addition";
	if ($finance->update_bank_baroda($total, $amount, $type)){
		$balance=$finance->check_bank_baroda_balance();
		if ($finance->enter_loan($amount, $date, $interest)){
			echo "<META HTTP-EQUIV=\"Refresh\" CONTENT=\"1;	URL=loan.php?m=success&balance=$balance\">";
		}
	}
}
else if($account=="HFCK Bank account"){
	#update account
	$total=$finance->check_bank_hfck_balance()+$amount;
	$type="Addition";
	if ($finance->update_bank_hfck($total, $amount, $type)){
		$balance=$finance->check_bank_hfck_balance();
		if ($finance->enter_loan($amount, $date, $interest)){
			echo "<META HTTP-EQUIV=\"Refresh\" CONTENT=\"1;	URL=loan.php?m=success&balance=$balance\">";
		}
	}
}
else if($option=="Rafiki DTM account"){
	#update account
	$total=$finance->check_bank_rafiki_balance()+$amount;
	$type="Addition";
	if ($finance->update_bank_rafiki($total, $amount, $type)){
		$balance=$finance->check_bank_rafiki_balance();
		if ($finance->enter_loan($amount, $date, $interest)){
			echo "<META HTTP-EQUIV=\"Refresh\" CONTENT=\"1;	URL=loan.php?m=success&balance=$balance\">";
		}
	}
}

?>
