<a href="#dashboard-menu" class="nav-header collapsed" data-toggle="collapse"><i class="icon-dashboard"></i>Dashboard</a>
        <ul id="dashboard-menu" class="nav nav-list collapse">
            <li><a href="../dashboard.php">Home</a></li>
            <!--<li ><a href="users.html">Recent Jobs</a></li>
            <li ><a href="user.html">Recent clients</a></li>-->
        </ul>

        <a href="#finance-menu" class="nav-header collapsed" data-toggle="collapse"><i class="icon-money"></i>Finance </a>
        <ul id="finance-menu" class="nav nav-list collapse">
			<li ><a href="../finance/assets.php">Assets</a></li>
			<li ><a href="../finance/payment.php?id=0">Add Miscelaneous income</a></li>
			<li ><a href="../finance/expenses.php?id=1">Admin expenses</a></li>
			<li ><a href="../account_details.php">Account details</a></li>
			<li ><a href="../finance/loan_user_biz.php">Loan:user->biz</a></li>
			<li ><a href="../finance/loan_biz_user.php">Loan:biz->user</a></li>
			<li ><a href="../users/user_loans.php">User Loans</a></li>
			<li ><a href="../finance/biz_loans.php">Business Loans</a></li>
			<li ><a href="../finance/loan.php">Loan</a></li>
			<li ><a href="../finance/reimburse.php">Reimburse</a></li>
			<li ><a href="../finance/payables.php">Payables</a></li>
			<li ><a href="../finance/receivables.php">Receivables</a></li> 
        </ul>

        <a href="#stock-menu" class="nav-header" data-toggle="collapse"><i class="icon-briefcase"></i>Stock</a>
        <ul id="stock-menu" class="nav nav-list collapse">
            <li ><a href="#">Add Stock</a></li>
            <li ><a href="#">View Stock</a></li>
        </ul>

        <a href="#client-menu" class="nav-header" data-toggle="collapse" ><i class="icon-group"></i>Clients</a>
        <ul id="client-menu" class="nav nav-list collapse">
			<li><a href="../clients/new_client.php">New Client</a></li>
			<li><a href="../clients/view_clients.php">Client List</a></li>
        </ul>
        <a href="../users/user_list.php" class="nav-header" ><i class="icon-user-md"></i>Users</a>
        <a href="#reports-menu" class="nav-header" data-toggle="collapse"><i class="icon-file"></i>Reports</a>
        <ul id="reports-menu" class="nav nav-list collapse">
			<li ><a href="../reports/adminexpenses_report.php">Admin Expenses</a></li>
			<li ><a href="../reports/baroda_report.php">Bank of Baroda</a></li>
			<li ><a href="../reports/cashbox_report.php">Cashbox</a></li>
			<li ><a href="../reports/customer_list.php">Customer List Report</a></li>
			<li ><a href="../reports/hfck_report.php">HFCK Bank</a></li>
			<li ><a href="../reports/master_report.php">Master report</a></li>
            <li ><a href="../reports/opexpenses_report.php">Operating Expenses</a></li>
            <li ><a href="../reports/rafiki_report.php">Rafiki DTM</a></li>
            <li ><a href="../reports/revenue_analysis.php">Revenue Analysis</a></li>
            <li ><a href="../reports/sales_report.php">Total Sales</a></li>
        </ul>
