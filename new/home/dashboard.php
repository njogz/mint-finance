<?php
if (!isset($_COOKIE['id'])) {
	header("location:../index.php");
}
include ('config.php');
include ( ROOT_DIR.'../users/users.php' );
//include ("../users/users.php");
//include ("../jobs/jobs.php");

$user = new users;
$job = new jobs;
?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <title>Mintgraphics Finance</title>
    <meta content="IE=edge,chrome=1" http-equiv="X-UA-Compatible">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="Njuguna Ndung'u">

    <link rel="stylesheet" type="text/css" href="lib/bootstrap/css/bootstrap.css">
    
    <link rel="stylesheet" type="text/css" href="stylesheets/theme.css">
    <link rel="stylesheet" href="lib/font-awesome/css/font-awesome.css">

    <script src="lib/jquery-1.7.2.min.js" type="text/javascript"></script>

    <!-- Demo page code -->

    <style type="text/css">
        #line-chart {
            height:300px;
            width:800px;
            margin: 0px auto;
            margin-top: 1em;
        }
        .brand { font-family: georgia, serif; }
        .brand .first {
            color: #ccc;
            font-style: italic;
        }
        .brand .second {
            color: #fff;
            font-weight: bold;
        }
    </style>

    <!-- Le HTML5 shim, for IE6-8 support of HTML5 elements -->
    <!--[if lt IE 9]>
      <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->

    <!-- Le fav and touch icons -->
    <link rel="shortcut icon" href="../assets/ico/favicon.ico">
    <link rel="apple-touch-icon-precomposed" sizes="144x144" href="../assets/ico/apple-touch-icon-144-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="114x114" href="../assets/ico/apple-touch-icon-114-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="72x72" href="../assets/ico/apple-touch-icon-72-precomposed.png">
    <link rel="apple-touch-icon-precomposed" href="../assets/ico/apple-touch-icon-57-precomposed.png">
  </head>

  <!--[if lt IE 7 ]> <body class="ie ie6"> <![endif]-->
  <!--[if IE 7 ]> <body class="ie ie7 "> <![endif]-->
  <!--[if IE 8 ]> <body class="ie ie8 "> <![endif]-->
  <!--[if IE 9 ]> <body class="ie ie9 "> <![endif]-->
  <!--[if (gt IE 9)|!(IE)]><!--> 
  <body class=""> 
  <!--<![endif]-->
    
    <div class="navbar">
        <div class="navbar-inner">
                <ul class="nav pull-right">
                    
                    <!-- <li><a href="#" class="hidden-phone visible-tablet visible-desktop" role="button">Settings</a></li> -->
                    <li id="fat-menu" class="dropdown">
                        <a href="#" role="button" class="dropdown-toggle" data-toggle="dropdown">
                            <i class="icon-user"></i>
                            <?php
								$name = $user->get_user("user_name",$_COOKIE['id']);
								echo $name;                            
                            ?>
                            <i class="icon-caret-down"></i>
                        </a>

                        <ul class="dropdown-menu">
							<li><a tabindex="-1" href="#">Notifications</a></li>
                            <li class="divider"></li>
                            <li><a tabindex="-1" href="#">My Account</a></li>
                            <li class="divider"></li>
                            <!-- <li><a tabindex="-1" class="visible-phone" href="#">Settings</a></li> -->
                            <li class="divider visible-phone"></li>
                            <li><a tabindex="-1" href="users/logout.php">Logout</a></li>
                        </ul>
                    </li>
                    
                </ul>
                <a class="brand" href="index.html"><span class="first">Mint</span> <span class="second">Graphics</span></a>
        </div>
    </div>
    <div class="sidebar-nav">
        <form class="search form-inline">
            <input type="text" placeholder="Search...">
        </form>

        <a href="#dashboard-menu" class="nav-header collapsed" data-toggle="collapse"><i class="icon-dashboard"></i>Dashboard</a>
        <ul id="dashboard-menu" class="nav nav-list collapse">
            <li><a href="dashboard.php">Home</a></li>
            <!--<li ><a href="users.html">Recent Jobs</a></li>
            <li ><a href="user.html">Recent clients</a></li>-->
        </ul>

        <a href="#finance-menu" class="nav-header collapsed" data-toggle="collapse"><i class="icon-money"></i>Finance </a>
        <ul id="finance-menu" class="nav nav-list collapse">
			<li ><a href="finance/payment.php?id=0">Add Miscelaneous income</a></li>
			<li ><a href="finance/expenses.php?id=1">Admin expenses</a></li>
			<li ><a href="account_details.php">Account details</a></li>
			<li ><a href="finance/loan_user_biz.php">Loan:user->biz</a></li>
			<li ><a href="finance/loan_biz_user.php">Loan:biz->user</a></li>
			<li ><a href="users/user_loans.php">User Loans</a></li>
			<li ><a href="finance/biz_loans.php">Business Loans</a></li>
			<li ><a href="finance/loan.php">Loan</a></li>
			<li ><a href="finance/reimburse.php">Reimburse</a></li>
			<li ><a href="finance/payables.php">Payables</a></li>
        </ul>

        <a href="#stock-menu" class="nav-header" data-toggle="collapse"><i class="icon-briefcase"></i>Stock</a>
        <ul id="stock-menu" class="nav nav-list collapse">
            <li ><a href="#">Add Stock</a></li>
            <li ><a href="#">View Stock</a></li>
        </ul>

        <a href="#client-menu" class="nav-header" data-toggle="collapse" ><i class="icon-group"></i>Clients</a>
        <ul id="client-menu" class="nav nav-list collapse">
			<li><a href="clients/new_client.php">New Client</a></li>
			<li><a href="clients/view_clients.php">Client List</a></li>
        </ul>
        <a href="users/user_list.php" class="nav-header" ><i class="icon-user-md"></i>Users</a>
        <a href="#reports-menu" class="nav-header" data-toggle="collapse"><i class="icon-file"></i>Reports</a>
        <ul id="reports-menu" class="nav nav-list collapse">
            <li ><a href="reports/opexpenses_report.php">Operating Expenses</a></li>
            <li ><a href="reports/adminexpenses_report.php">Admin Expenses</a></li>
            <li ><a href="reports/sales_report.php">Total Sales</a></li>
            <li ><a href="reports/cashbox_report.php">Cashbox</a></li>
            <li ><a href="reports/baroda_report.php">Bank of Baroda</a></li>
            <li ><a href="reports/hfck_report.php">HFCK Bank</a></li>
            <li ><a href="reports/rafiki_report.php">Rafiki DTM</a></li>
        </ul>
    </div>
    

    
    <div class="content">
        
        <div class="header">
            
            <h1 class="page-title">Dashboard</h1>
        </div>
        
                <ul class="breadcrumb">
            <li><a href="index.html">Home</a> <span class="divider">/</span></li>
            <li class="active">Dashboard</li>
        </ul>

        <div class="container-fluid">
            <div class="row-fluid">
                    

<div class="row-fluid">

   <!-- <div class="alert alert-info">
        <button type="button" class="close" data-dismiss="alert">×</button>
        <strong>Just a quick note:</strong> Hope you like the theme!
    </div> -->

    <div class="block">
        <a href="#page-stats" class="block-heading" data-toggle="collapse">Latest Stats (this month)</a>
        <div id="page-stats" class="block-body collapse in">

            <div class="stat-widget-container">
                <div class="stat-widget">
                    <div class="stat-button">
                        <p class="title"><?php echo $job->get_latest_jobcards() ?></p>
                        <p class="detail">New Job Cards</p>
                    </div>
                </div>

                <div class="stat-widget">
                    <div class="stat-button">
						<?php
							$current_date = date("Y-m");
							$start_date = $current_date."-01";
							$end_date = $current_date."-31";
						?>
                        <p class="title"><?php echo "Ksh ".number_format($job->get_sales_period($start_date, $end_date), 2) ?></p>
                        <p class="detail">Total Sales</p>
                    </div>
                </div>

                <div class="stat-widget">
                    <div class="stat-button">
                        <p class="title"><?php echo "Ksh ".number_format($job->get_expenses_period($start_date, $end_date), 2) ?></p>
                        <p class="detail">Total Expenses</p>
                    </div>
                </div>

                <div class="stat-widget">
                    <div class="stat-button">
                        <p class="title"><?php echo "Ksh ".number_format($job->get_payments_period($start_date, $end_date), 2) ?></p>
                        <p class="detail">Total Paid</p>
                    </div>
                </div>

            </div>
        </div>
    </div>
</div>

<div class="row-fluid">
    <div class="block span6">
        <a href="#tablewidget" class="block-heading" data-toggle="collapse">Job cards this month</a>
        <div id="tablewidget" class="block-body collapse in">
            <table class="table table-bordered">
              <thead>
                <tr>
                  <th>Name</th>
                  <th>Total</th>
                  <th>Amount paid</th>
                </tr>
              </thead>
              <tbody>
				  <?php echo $job->get_current_jobcards(); ?>
              </tbody>
            </table>
        </div>
    </div>
    <div class="block span6">
        <a href="#widget1container" class="block-heading" data-toggle="collapse">Recent expenses this month </a>
        <div id="widget1container" class="block-body collapse in">
            <table class="table table-bordered">
              <thead>
                <tr>
                  <th>Type</th>
                  <th>Amount</th>
                  <th>Details</th>
                </tr>
              </thead>
              <tbody>
				  <?php echo $job->get_current_expenses(); ?>
              </tbody>
            </table>
        </div>
    </div>
</div>

<div class="row-fluid">
    <div class="block span6">
        <div class="block-heading">
            <a href="#widget2container" data-toggle="collapse">Recent payments this month</a>
        </div>
        <div id="widget2container" class="block-body collapse in">
            <table class="table table-bordered">
              <thead>
                <tr>
                  <th>Amount</th>
                  <th>Date</th>
                </tr>
              </thead>
              <tbody>
				  <?php echo $job->get_current_payments(); ?>
              </tbody>
            </table>
        </div>
    </div>
</div>


                    
                    <footer>
                        <hr>
                        
                        <p>&copy; <?php echo date('Y')?> Mint Graphics</p>
                    </footer>
                    
            </div>
        </div>
    </div>
    


    <script src="lib/bootstrap/js/bootstrap.js"></script>
    <script type="text/javascript">
        $("[rel=tooltip]").tooltip();
        $(function() {
            $('.demo-cancel-click').click(function(){return false;});
        });
    </script>
    
  </body>
</html>


