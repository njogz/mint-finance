<?php
if (!isset($_COOKIE['id'])) {
	header("location:index.php");
}
include "users/users.php";
include "finance/finance.php";

$user = new users;
$finance = new finance;
?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <title>Mintgraphics Finance</title>
    <meta content="IE=edge,chrome=1" http-equiv="X-UA-Compatible">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="Njuguna Ndung'u">

        <link rel="stylesheet" type="text/css" href="lib/bootstrap/css/bootstrap.css">
    
    <link rel="stylesheet" type="text/css" href="stylesheets/theme.css">
    <link rel="stylesheet" href="lib/font-awesome/css/font-awesome.css">
    <link rel="stylesheet" href="http://code.jquery.com/ui/1.10.3/themes/smoothness/jquery-ui.css">

    <script src="lib/jquery-1.7.2.min.js" type="text/javascript"></script>
    <script src="http://code.jquery.com/jquery-1.9.1.js"></script>
	<script src="http://code.jquery.com/ui/1.10.3/jquery-ui.js"></script>
	<script>
	$(function() {
		$( "#startdate" ).datepicker();
	 });
	 </script>

    <!-- Demo page code -->

    <style type="text/css">
        #line-chart {
            height:300px;
            width:800px;
            margin: 0px auto;
            margin-top: 1em;
        }
        .brand { font-family: georgia, serif; }
        .brand .first {
            color: #ccc;
            font-style: italic;
        }
        .brand .second {
            color: #fff;
            font-weight: bold;
        }
    </style>

    <!-- Le HTML5 shim, for IE6-8 support of HTML5 elements -->
    <!--[if lt IE 9]>
      <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->

    <!-- Le fav and touch icons -->
    <link rel="shortcut icon" href="../assets/ico/favicon.ico">
    <link rel="apple-touch-icon-precomposed" sizes="144x144" href="../assets/ico/apple-touch-icon-144-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="114x114" href="../assets/ico/apple-touch-icon-114-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="72x72" href="../assets/ico/apple-touch-icon-72-precomposed.png">
    <link rel="apple-touch-icon-precomposed" href="../assets/ico/apple-touch-icon-57-precomposed.png">
  </head>

  <!--[if lt IE 7 ]> <body class="ie ie6"> <![endif]-->
  <!--[if IE 7 ]> <body class="ie ie7 "> <![endif]-->
  <!--[if IE 8 ]> <body class="ie ie8 "> <![endif]-->
  <!--[if IE 9 ]> <body class="ie ie9 "> <![endif]-->
  <!--[if (gt IE 9)|!(IE)]><!--> 
  <body class=""> 
  <!--<![endif]-->
    
    <div class="navbar">
        <div class="navbar-inner">
                <ul class="nav pull-right">
                    
                    <!-- <li><a href="#" class="hidden-phone visible-tablet visible-desktop" role="button">Settings</a></li> -->
                    <li id="fat-menu" class="dropdown">
                        <a href="#" role="button" class="dropdown-toggle" data-toggle="dropdown">
                            <i class="icon-user"></i>
                            <?php
								$name = $user->get_user("user_name",$_COOKIE['id']);
								echo $name;                            
                            ?>
                            <i class="icon-caret-down"></i>
                        </a>

                        <ul class="dropdown-menu">
							<li><a tabindex="-1" href="users/notifications.php?id=<?php echo $_COOKIE['id']; ?>">Notifications</a></li>
                            <li class="divider"></li>
                            <li><a tabindex="-1" href="users/account.php">My Account</a></li>
                            <li class="divider"></li>
                            <!-- <li><a tabindex="-1" class="visible-phone" href="#">Settings</a></li> -->
                            <li class="divider visible-phone"></li>
                            <li><a tabindex="-1" href="users/logout.php">Logout</a></li>
                        </ul>
                    </li>
                    
                </ul>
                <a class="brand" href="index.html"><span class="first">Mint</span> <span class="second">Graphics</span></a>
        </div>
    </div>
    <div class="sidebar-nav">
        <form class="search form-inline">
            <input type="text" placeholder="Search...">
        </form>

        <?php
			define('ROOT', getcwd());
			require_once ROOT . '/menu.php';
		?>
    </div>
    

    
    <div class="content">
        
        <div class="header">
            
            <h1 class="page-title">Account details</h1>
        </div>
        
                <ul class="breadcrumb">
            <li><a href="dashboard.php">Home</a> <span class="divider">/</span></li>
            <li class="active">Dashboard</li>
        </ul>

        <div class="container-fluid">
            <div class="row-fluid">
                    

<div class="row-fluid">

   <!-- <div class="alert alert-info">
        <button type="button" class="close" data-dismiss="alert">×</button>
        <strong>Just a quick note:</strong> Hope you like the theme!
    </div> -->
    <?php
		if(isset($_POST['startdate'])){
			$start_date=$_POST['startdate'];
			list($start_month,$start_date,$start_year)=explode("/",$start_date);
			$startdate=$start_year."-".$start_month."-".$start_date;
			echo '<div class="alert alert-info">
				<button type="button" class="close" data-dismiss="alert">×</button>
				Account totals for '.$startdate.'
			</div>';
		}
	?>
    
    <form method="post" action="account_details.php">
	Start Date: <input type="text" name="startdate" id="startdate" />  <input type="submit" class="btn btn-primary" value="Get Balances" />
	</form>

    <div class="block ">
        	<table   class="table table-bordered">
				<thead>
					<tr><td>Name</td><td>Current balance (KSh)</td></tr>
				</thead>
				<tbody>
					<?php
					if(isset($_POST['startdate'])){ ?>
						<tr><td>Bank of Baroda</td><td><?php echo number_format($finance->account_balance("bank_baroda",$startdate),2); ?></td></tr>
						<tr><td>HFCK Bank</td><td><?php echo number_format($finance->account_balance("bank_hfck",$startdate),2); ?></td></tr>
						<tr><td>Rafiki DTM</td><td><?php echo number_format($finance->account_balance("bank_rafiki",$startdate),2); ?></td></tr>
						<tr><td>Cashbox</td><td><?php echo number_format($finance->account_balance("cashbox",$startdate),2); ?></td></tr>
					<?php
					}else{
					?>
					<tr><td>Bank of Baroda</td><td><?php echo number_format($finance->check_bank_baroda_balance(),2); ?></td></tr>
					<tr><td>HFCK Bank</td><td><?php echo number_format($finance->check_bank_hfck_balance(),2); ?></td></tr>
					<tr><td>Rafiki DTM</td><td><?php echo number_format($finance->check_bank_rafiki_balance(),2); ?></td></tr>
					<tr><td>Cashbox</td><td><?php echo number_format($finance->check_cashbox_balance(),2); ?></td></tr>
					<?php } ?>
				</tbody>
			</table>
    </div>
    <a href="finance/transfer.php" class="btn btn-primary">Transfer Cash</a>
</div>

                    
                    <footer>
                        <hr>
                        
                        <p>&copy; <?php echo date('Y')?> Mint Graphics</p>
                    </footer>
                    
            </div>
        </div>
    </div>
    


    <script src="lib/bootstrap/js/bootstrap.js"></script>
    <script type="text/javascript">
        $("[rel=tooltip]").tooltip();
        $(function() {
            $('.demo-cancel-click').click(function(){return false;});
        });
    </script>
    
  </body>
</html>


