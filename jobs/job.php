<?php
if (!isset($_COOKIE['id'])) {
	header("location:../index.php");
}
include "../users/users.php";
include "../clients/clients.php";
include "jobs.php";

$user = new users;
$client = new clients;
$job = new jobs;
$job_card_id=$_GET['id'];
$client_id=$job->get_card_field('client_id', $job_card_id);
?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <title>Mintgraphics Finance</title>
    <meta content="IE=edge,chrome=1" http-equiv="X-UA-Compatible">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="Njuguna Ndung'u">

    <link rel="stylesheet" type="text/css" href="../lib/bootstrap/css/bootstrap.css">
    
    <link rel="stylesheet" type="text/css" href="../stylesheets/theme.css">
    <link rel="stylesheet" href="../lib/font-awesome/css/font-awesome.css">

    <script src="../lib/jquery-1.7.2.min.js" type="text/javascript"></script>

    <!-- Demo page code -->

    <style type="text/css">
        #line-chart {
            height:300px;
            width:800px;
            margin: 0px auto;
            margin-top: 1em;
        }
        .brand { font-family: georgia, serif; }
        .brand .first {
            color: #ccc;
            font-style: italic;
        }
        .brand .second {
            color: #fff;
            font-weight: bold;
        }
    </style>

    <!-- Le HTML5 shim, for IE6-8 support of HTML5 elements -->
    <!--[if lt IE 9]>
      <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->

    <!-- Le fav and touch icons -->
    <link rel="shortcut icon" href="../assets/ico/favicon.ico">
    <link rel="apple-touch-icon-precomposed" sizes="144x144" href="../../assets/ico/apple-touch-icon-144-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="114x114" href="../../assets/ico/apple-touch-icon-114-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="72x72" href="../../assets/ico/apple-touch-icon-72-precomposed.png">
    <link rel="apple-touch-icon-precomposed" href="../../assets/ico/apple-touch-icon-57-precomposed.png">
  </head>

  <!--[if lt IE 7 ]> <body class="ie ie6"> <![endif]-->
  <!--[if IE 7 ]> <body class="ie ie7 "> <![endif]-->
  <!--[if IE 8 ]> <body class="ie ie8 "> <![endif]-->
  <!--[if IE 9 ]> <body class="ie ie9 "> <![endif]-->
  <!--[if (gt IE 9)|!(IE)]><!--> 
  <body class=""> 
  <!--<![endif]-->
    
    <div class="navbar">
        <div class="navbar-inner">
                <ul class="nav pull-right">
                    
                    <!-- <li><a href="#" class="hidden-phone visible-tablet visible-desktop" role="button">Settings</a></li> -->
                    <li id="fat-menu" class="dropdown">
                        <a href="#" role="button" class="dropdown-toggle" data-toggle="dropdown">
                            <i class="icon-user"></i>
                            <?php
								$name = $user->get_user("user_name",$_COOKIE['id']);
								echo $name;                            
                            ?>
                            <i class="icon-caret-down"></i>
                        </a>

                        <ul class="dropdown-menu">
							<?php
								define('ROOT', getcwd());
								require_once ROOT . '../../menu/usermenu.php';
							?>
                        </ul>
                    </li>
                    
                </ul>
                <a class="brand" href="index.html"><span class="first">Mint</span> <span class="second">Graphics</span></a>
        </div>
    </div>
    <div class="sidebar-nav">
        <form class="search form-inline">
            <input type="text" placeholder="Search...">
        </form>

        <?php
			define('ROOT', getcwd());
			require_once ROOT . '../../menu/sidemenu.php';
		?>
    </div>
    

    
    <div class="content">
        
        <div class="header">
            <?php $card_name=$job->get_card_field('name', $job_card_id); ?>
            <h1 class="page-title"><?php echo $card_name; ?> details</h1>
        </div>
        
                <ul class="breadcrumb">
            <li><a href="../dashboard.php">Home</a> <span class="divider">/</span></li>
            <li class="active">Clients <span class="divider">/</span></li>
            <li><a href="../clients/client.php?id=<?php echo $client_id; ?>"><?php echo $client->get_client("first_name",$client_id); ?></a> <span class="divider">/</span></li>
            <li class="active">job card details</li>
        </ul>

        <div class="container-fluid">
            <div class="row-fluid">
                    

<div class="row-fluid">
	<?php
	if(isset($_GET['m'])){
		if($_GET['m']=='error'){
			$amountpayable=$_GET['amount'];
			//$message="The amount should not be more than Ksh".$amountpayable." \n Please close this window and enter again";
			echo '<div class="alert alert-error">
			<button type="button" class="close" data-dismiss="alert">×</button>
			Something went wrong. Please try again.
			</div>';
		}
		elseif($_GET['m']=='success'){
			echo '<div class="alert alert-success">
			<button type="button" class="close" data-dismiss="alert">×</button>
			Job successfuly added
			</div>';
		}
		elseif($_GET['m']=='card_success'){
			echo '<div class="alert alert-success">
			<button type="button" class="close" data-dismiss="alert">×</button>
			Job card successfuly added. <strong>Add individual jobs</strong>
			</div>';
		}
	}
	?>
    <div class="block ">
		<div class="bold_info">
			<p>Date Started: <span  class="text-info"><?php echo $job->get_card_field("date_created",$job_card_id); ?></span></p>
			<p>Status: <span  class="text-info"><?php echo $job->get_card_field("status",$job_card_id); ?></span></p>
			<p>Comments: <span  class="text-info"><?php echo $job->get_card_field("comments",$job_card_id); ?></span></p>
			<?php 
			$total=$job->get_card_field('total', $job_card_id);
			$vat_total=(0.16*$total)+$total;
			?>
			<p>Total (inc VAT): <span  class="text-info"><?php echo number_format($vat_total,2); ?></span></p>
			<p>Total expenses: <span  class="text-info"><?php echo number_format($job->get_card_expenses($job_card_id),2); ?></span></p> 
			<?php
				$difference=$vat_total-$job->get_card_expenses($job_card_id);
				if($difference<0){
					$profit="<b style='color:red'>".number_format($difference,2)."</b>";
				}
				else{
					$profit="<b style='color:green'>".number_format($difference,2)."</b>";
				}
			?>
			<p>Profit: <?php echo $profit; ?></p>
			<p>Amount Paid: <span  class="text-info"><?php echo number_format($job->get_card_field('amount_paid', $job_card_id),2); ?></span></p>
		</div>
		
		<table  class="table table-bordered">
			<thead>
				<tr><td>Particulars</td><td>Quantity</td><td>Amount</td><td>Add stock</td><td>Edit</td></tr>
			</thead>
			<tbody>
				<?php echo $job->get_card_jobs($job_card_id); ?>
			</tbody>
		</table>
		<p><a class="btn btn-primary" href='add_job.php?id=<?php echo $job_card_id; ?>'>Add job</a></p>
    </div>
</div>

                    
                    <footer>
                        <hr>
                        
                        <p>&copy; <?php echo date('Y')?> Mint Graphics</p>
                    </footer>
                    
            </div>
        </div>
    </div>
    
    <script src="../lib/bootstrap/js/bootstrap.js"></script>
    <script type="text/javascript">
        $("[rel=tooltip]").tooltip();
        $(function() {
            $('.demo-cancel-click').click(function(){return false;});
        });
    </script>
    
  </body>
</html>
