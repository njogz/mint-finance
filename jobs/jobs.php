<?php
include_once "../users/finance_config.php";
include_once "../clients/clients.php";

class jobs{
	
	public function jobs(){
		include_once "../clients/clients.php";
	}
	
	public function add_job_card($name,$client,$comments,$lpo){
		$status="incomplete";
		$date=date("Y-m-d");
		$expiry_date=date('Y-m-d', strtotime("+90 days"));
		$query = "INSERT INTO job_card (name,status,client_id,date_created,comments,lpo,expiry_date) VALUES ('$name','$status','$client','$date','$comments','$lpo','$expiry_date')";
		$result = mysql_query($query) or die(mysql_error());
		if ($result){
			return TRUE;
		}
		else{
			return FALSE;
		}
	}
	
	public function add_job($card_id,$particulars,$quantity,$amount){
		$query = "INSERT INTO job (card_id,particulars,quantity,amount) VALUES ('$card_id','$particulars','$quantity','$amount')";
		$result = mysql_query($query);
		if ($result){
			return TRUE;
		}
		else{
			return FALSE;
		}
	}
	
	public function create_invoice($client_id,$card_id){
		$query = "INSERT INTO invoices (client_id,job_id,status) values ('$client_id','$card_id','unpaid')";
		$result = mysql_query($query);
		if ($result){
			$this->update_card_invoice($card_id);
			return TRUE;
		}
		else{
			return FALSE;
		}
	}
	
	public function update_card_invoice($card_id){
		$query = "SELECT invoice_id from invoices WHERE job_id=$card_id";
		$result = mysql_query($query);
		while ($job=mysql_fetch_row($result)){
			$field=$job['0'];
		}
		$invoice=$field;
		$query1 = "UPDATE job_card SET invoice_id=$invoice WHERE card_id=$card_id";
		$result1 = mysql_query($query1);
		if ($result1){
			return TRUE;
		}
		else{
			return FALSE;
		}
	}
	
	public function get_card_number($client){
		$query = "SELECT card_id from  job_card WHERE client_id=$client ORDER BY card_id ASC";
		$result = mysql_query($query);
		while ($user=mysql_fetch_row($result)){
			$field=$user['0'];
		}
		return $field;
	}
	
	public function get_card_status($card_id){
		$query = "SELECT status from  job_card WHERE card_id=$card_id";
		$result = mysql_query($query);
		while ($status=mysql_fetch_row($result)){
			$field=$status['0'];
		}
		return $field;
	}
	
	public function get_card_field($field, $card_id){
		$query = "SELECT $field from  job_card WHERE card_id=$card_id";
		$result = mysql_query($query);
		while ($user=mysql_fetch_row($result)){
			$field=$user['0'];
		}
		return $field;
	}
	
	public function get_client_details($field, $client_id){
		$query = "SELECT $field from  clients WHERE client_id=$client_id";
		$result = mysql_query($query);
		while ($user=mysql_fetch_row($result)){
			$field=$user['0'];
		}
		return $field;
	}
	
	public function get_job_field($field, $job_id){
		$query = "SELECT $field from  job WHERE job_id=$job_id";
		$result = mysql_query($query);
		while ($user=mysql_fetch_row($result)){
			$field=$user['0'];
		}
		return $field;
	}
	
	public function get_change_field($field, $change_id){
		$query = "SELECT $field from  pending_changes WHERE change_id=$change_id";
		$result = mysql_query($query);
		while ($user=mysql_fetch_row($result)){
			$field=$user['0'];
		}
		return $field;
	}
	
	public function get_card_expenses($card_id){
		$query = "SELECT SUM(amount) FROM expenses WHERE job_id=$card_id";
		$result = mysql_query($query);
		while ($user=mysql_fetch_row($result)){
			$field=$user['0'];
		}
		return $field;
	}
	
	public function get_all_jobs($card_id){
		$query = "SELECT * FROM job WHERE card_id=$card_id";
		$result = mysql_query($query);
		$counter = 1;
		while ($job=mysql_fetch_object($result)){
			$jobs .='<tr><td style="width:8%;">'.$counter.'</td>
			<td style="text-align:left; padding-left:10px;">'.$job->particulars.'</td>
			<td class="mono" style="width:15%;">'.$job->quantity.'</td>
			<td style="width:15%;" class="mono">'.$job->amount.'</td>
			<td style="width:15%;" class="mono">'.$job->quantity*$job->amount.'</td></tr>';
			$counter++;
		}
		return $jobs;
	}
	
	public function get_all_jobs_delivered($card_id){
		$query = "SELECT * FROM job WHERE card_id=$card_id";
		$result = mysql_query($query);
		$counter = 1;
		while ($job=mysql_fetch_object($result)){
			$jobs .='<tr><td style="width:8%;">'.$counter.'</td>
			<td style="text-align:left; padding-left:10px;">'.$job->particulars.'</td>
			<td class="mono" style="width:15%;">'.$job->quantity.'</td></tr>';
			$counter++;
		}
		return $jobs;
	}
	
	public function get_card_jobs($card_id){
		$query = "SELECT * FROM job WHERE card_id=$card_id";
		$result = mysql_query($query);
		while ($job=mysql_fetch_object($result)){
			$jobs .='<tr><td>'.$job->particulars.'</td>
			<td>'.$job->quantity.'</td>
			<td>'.$job->amount.'</td>
			<td><a class="btn btn-primary">Add stock</a></td>
			<td><a class="btn btn-primary">Edit</a></td></tr>';
		}
		return $jobs;
	}
	
	public function update_card_total($card,$total){
		$query = "SELECT total from job_card WHERE card_id=$card";
		$result = mysql_query($query);
		while ($job=mysql_fetch_row($result)){
			$field=$job['0'];
		}
		$new_total=$field+$total;
		$query1 = "UPDATE job_card SET total=$new_total WHERE card_id=$card";
		$result1 = mysql_query($query1);
		if ($result1){
			return TRUE;
		}
		else{
			return FALSE;
		}
	}
	
	public function update_card_paid($card,$total){
		$query = "SELECT amount_paid from job_card WHERE card_id=$card";
		$result = mysql_query($query);
		while ($job=mysql_fetch_row($result)){
			$field=$job['0'];
		}
		$new_total=$field+$total;
		$query1 = "UPDATE job_card SET amount_paid=$new_total WHERE card_id=$card";
		$result1 = mysql_query($query1);
		if ($result1){
			return TRUE;
		}
		else{
			return FALSE;
		}
	}
	
	public function update_invoice_amount($card,$total){
		$query = "SELECT amount from invoices WHERE job_id=$card";
		$result = mysql_query($query);
		while ($job=mysql_fetch_row($result)){
			$field=$job['0'];
		}
		$new_total=$field+$total;
		$query1 = "UPDATE invoices SET amount=$new_total WHERE job_id=$card";
		$result1 = mysql_query($query1);
		if ($result1){
			return TRUE;
		}
		else{
			return FALSE;
		}
	}
	
	public function update_client_jobs($client){
		$query = "SELECT jobs from clients WHERE client_id=$client";
		$result = mysql_query($query);
		while ($job=mysql_fetch_row($result)){
			$field=$job['0'];
		}
		$jobs=$field+1;
		$query1 = "UPDATE clients SET jobs=$jobs WHERE client_id=$client";
		$result1 = mysql_query($query1);
		if ($result1){
			return TRUE;
		}
		else{
			return FALSE;
		}
	}
	
	public function get_latest_jobcards(){
		$current_date = date("Y-m");
		$start_date = $current_date."-01";
		$query = "SELECT COUNT(*) as count FROM job_card WHERE date_created >= '$start_date'";
		$result = mysql_query($query);
		$data = mysql_fetch_assoc($result);
		
		return $data['count'];
	}
	
	public function get_current_jobcards(){
		$current_date = date("Y-m");
		$start_date = $current_date."-01";
		$query = "SELECT name, total, amount_paid FROM job_card WHERE date_created >= '$start_date'";
		$result = mysql_query($query);
		while ($card=mysql_fetch_row($result)){
			$total=$card['1'];
			$vat_total=(0.16*$total)+$total;
			$data .= "<tr>
					<td>".$card['0']."</td><td>".$vat_total."</td><td>".$card['2']."</td>
					</tr>";
		}
		return $data;
	}
	
	public function get_current_expenses(){
		$current_date = date("Y-m");
		$start_date = $current_date."-01";
		$query = "SELECT type, amount, details FROM expenses WHERE date >= '$start_date' ORDER BY expense_id DESC LIMIT 0, 10";
		$result = mysql_query($query);
		while ($expense=mysql_fetch_row($result)){
			$data .= "<tr>
					<td>".$expense['0']."</td><td>".$expense['1']."</td><td>".$expense['2']."</td>
					</tr>";
		}
		return $data;
	}
	
	public function get_current_payments(){
		$current_date = date("Y-m");
		$start_date = $current_date."-01";
		$query = "SELECT amount, date FROM payments WHERE date >= '$start_date' ORDER BY payment_id DESC LIMIT 0, 10";
		$result = mysql_query($query);
		while ($expense=mysql_fetch_row($result)){
			$data .= "<tr>
					<td>".$expense['0']."</td><td>".$expense['1']."</td>
					</tr>";
		}
		return $data;
	}
	
	public function get_sales_period($start_date, $end_date){
		$query = "SELECT SUM(total) as sales FROM job_card WHERE date_created >= '$start_date' AND date_created <= '$end_date'";
		$result = mysql_query($query);
		$data = mysql_fetch_assoc($result);
		
		$vat_total = ($data['sales']*0.16)+$data['sales'];
		
		return $vat_total;
	}
	
	public function get_expenses_period($start_date, $end_date){
		$query = "SELECT SUM(amount) as expenses FROM expenses WHERE date >= '$start_date' AND date <= '$end_date'";
		$result = mysql_query($query);
		$data = mysql_fetch_assoc($result);
		
		return $data['expenses'];
	}
	
	public function get_payments_period($start_date, $end_date){
		$query = "SELECT SUM(amount) as payments FROM payments WHERE date >= '$start_date' AND date <= '$end_date'";
		$result = mysql_query($query);
		$data = mysql_fetch_assoc($result);
		
		return $data['payments'];
	}
	public function get_sales_list_period($start_date, $end_date){
		$client=new clients;
		$query="SELECT * FROM job_card WHERE date_created >= '$start_date' AND date_created <= '$end_date' ORDER BY card_id DESC";
		$result = mysql_query($query);
		while ($sales=mysql_fetch_object($result)){
		  echo "<tr>";
			echo "<th scope='row' align='center'>".$sales->date_created."</th>";
			echo "<td align='center'>".$client->get_client("first_name",$sales->client_id)." ".$client->get_client("last_name",$sales->client_id)."</td>";
			echo "<td align='center'>".$sales->name."</td>";
			$total=((0.16)*($sales->total))+$sales->total;
			echo "<td align='center'>".number_format($total,2)."</td>";
			echo "<td align='center'>".number_format($sales->amount_paid,2)."</td>";
			$amount_owed=number_format($total-$sales->amount_paid,2);
			echo "<td width='100' align='center'>".$amount_owed."</td>";
		  echo "</tr>";
		}
	}
	public function get_sales_total_period($start_date, $end_date){
		$query="SELECT SUM(total) AS total FROM job_card WHERE date_created >= '$start_date' AND date_created <= '$end_date'";
		$result=mysql_query($query);
		while ($total=mysql_fetch_object($result)){
		$salestotal=(0.16*$total->total)+$total->total;
		}
		$query="SELECT SUM(amount_paid) AS paid FROM job_card WHERE date_created >= '$start_date' AND date_created <= '$end_date'";
		$result=mysql_query($query);
		while ($total=mysql_fetch_object($result)){
		$paid=$total->paid;
		}
		$owed=$salestotal-$paid;
		echo "<tr>";
			echo "<td colspan='3' align='center'><b>Totals</b></td>";
			echo "<td align='center'><b>".number_format($salestotal,2)."</b></td>";
			echo "<td align='center'><b>".number_format($paid,2)."</b></td>";
			echo "<td align='center'><b>".number_format($owed,2)."</b></td>";
		  echo "</tr>";
		echo "</tbody>";
		echo "</table>";
	}
	public function deliver_job($card_id){
		$date=date("Y-m-d");
		$expiry_date=date('Y-m-d',strtotime("+ 90 day"));
		$query = "UPDATE job_card SET status='delivered', expiry_date='$expiry_date' WHERE card_id=$card_id";
		$result = mysql_query($query);
		if ($result){
			return TRUE;
		}
		else{
			return FALSE;
		}
	}
	
}
?>
