<?php
#set all variables from the form
	include "jobs.php";
	$jobs = new jobs;
	$name=$_POST['job_name'];
	$client=$_POST['client'];
	$comments=$_POST['comments'];
	$lpo=$_POST['lpo'];
#add new job card for the client
if($jobs->add_job_card($name,$client,$comments,$lpo)){
	#create an invoice for the job card
	$jobs->create_invoice($client,$jobs->get_card_number($client));
	
	#update client job count
	$jobs->update_client_jobs($client);
	
	#redirect to the job card page
	$card_id=$jobs->get_card_number($client);
	echo "<META HTTP-EQUIV=\"Refresh\" CONTENT=\"1; URL=job.php?id=$card_id&m=card_success\">";
}
else{
	echo "<META HTTP-EQUIV=\"Refresh\" CONTENT=\"1; URL=add_job_card.php?id=$client&m=error\">";
}
?>
