<?php $style='	<style>
		*
		{
			margin:0;
			padding:0;
			font-family:Arial;
			font-size:10pt;
			color:#000;
		}
		body
		{
			width:100%;
			font-family:Arial;
			font-size:10pt;
			margin:0;
			padding:0;
		}
		
		p
		{
			margin:0;
			padding:0;
		}
		
		#wrapper
		{
			width:180mm;
			margin:0 15mm;
		}
		
		.page
		{
			height:297mm;
			width:210mm;
			page-break-after:always;
		}

		table
		{
			border-left: 1px solid #ccc;
			border-top: 1px solid #ccc;
			
			border-spacing:0;
			border-collapse: collapse; 
			
		}
		
		table td 
		{
			border-right: 1px solid #ccc;
			border-bottom: 1px solid #ccc;
			padding: 2mm;
		}
		
		table.heading
		{
			height:50mm;
		}
		
		h1.heading
		{
			font-size:14pt;
			color:#000;
			font-weight:normal;
		}
		
		h2.heading
		{
			font-size:9pt;
			color:#000;
			font-weight:normal;
		}
		
		hr
		{
			color:#ccc;
			background:#ccc;
		}
		
		#invoice_body
		{
			height: 119mm;
		}
		
		#invoice_body , #invoice_total
		{	
			width:100%;
		}
		#invoice_body table , #invoice_total table
		{
			width:100%;
			border-left: 1px solid #ccc;
			border-top: 1px solid #ccc;
	
			border-spacing:0;
			border-collapse: collapse; 
			
			margin-top:5mm;
		}
		
		#invoice_body table td , #invoice_total table td
		{
			text-align:center;
			font-size:9pt;
			border-right: 1px solid #ccc;
			border-bottom: 1px solid #ccc;
			padding:2mm 0;
		}
		
		#invoice_body table td.mono  , #invoice_total table td.mono
		{
			font-family:monospace;
			text-align:right;
			padding-right:3mm;
			font-size:10pt;
		}
		
		#footer
		{	
			width:180mm;
			margin:0 15mm;
			padding-bottom:3mm;
		}
		#footer table
		{
			width:100%;
			border-left: 1px solid #ccc;
			border-top: 1px solid #ccc;
			
			background:#eee;
			
			border-spacing:0;
			border-collapse: collapse; 
		}
		#footer table td
		{
			width:25%;
			text-align:left;
			font-size:9pt;
			border-right: 1px solid #ccc;
			border-bottom: 1px solid #ccc;
		}
	</style>'; 
include "../jobs/jobs.php";
$job = new jobs;
$client = new clients;
$job_id = 5;
$client_id = $_GET['id'];
$buyer_name = $client->get_client("first_name",$client_id)." ".$client->get_client("last_name",$client_id);
$buyer_email = $client->get_client("email",$client_id);
$buyer_company = $client->get_client("company",$client_id);
$invoice_id = $job->get_card_field("invoice_id",$job_id);
$total = $job->get_card_field("total",$job_id);
$amount_paid = $job->get_card_field("amount_paid",$job_id);
$vat = 0.16*($total);
$newtotal = $total + $vat;
$amount_due = $newtotal - $amount_paid;
$html= $style.' <div id="wrapper">
    
    <p style="text-align:center; font-weight:bold; padding-top:5mm;">CUSTOMER STATEMENT</p>
    <br />
    <table class="heading" style="width:100%;">
    	<tr>
    		<td style="width:80mm;">
    			<img src="../images/MGEA.jpg" />
			</td>
			<td rowspan="2" valign="top" align="right" style="padding:3mm;">
				<table>
					<tr><td>Date : </td><td>'.date("d-m-Y").'</td></tr>
				</table>
			</td>
		</tr>
    	<tr>
    		<td>
    			Name: '.$buyer_name.'<br />
    			Company: '.$buyer_company.'<br />
    		</td>
    	</tr>
    </table>
		<br>
		<b>RE: <u>CUSTOMER STATEMENT</u></b><br>
		As regards services offered, we hereby state the paid and outstanding balance:
	<div id="content">
		
		<div id="invoice_body">
			<table>
			<tr style="background:#eee;">
				<td style="width:8%;"><b>LPO. No.</b></td>
				<td><b>Amount</b></td>
				<td style="width:15%;"><b>VAT</b></td>
				<td style="width:15%;"><b>Paid</b></td>
				<td style="width:15%;"><b>Balance</b></td>
			</tr>'.$client->customer_statement($client_id).'
			</table>
		</div>
		<br />
		<hr />
		<br />
		
		<table style="width:100%; height:35mm;" border="0">
			<tr>
				<td style="width:65%;" valign="top">
					<b>Payable to:</b> Mint Graphics East Africa Ltd<br />
					<b>PIN:</b> P051388193U<br>
					<b>E &amp; O.E.</b>
					
				</td>
				<td>
				</td>
			</tr>
		</table>
	</div>
	<br />
	</div>
	
	<htmlpagefooter name="footer">
		<hr />
		<div id="footer">	
			<table>
				<tr><td>P.O.Box: 7429-00200, Nairobi<br>
				Wireless: +254 20 269 5087<br>
				Cell: +254 773 745 753<br>
				Email: info@mintgraphics.co.ke
				</td></tr>
			</table>
		</div>
	</htmlpagefooter>
	<sethtmlpagefooter name="footer" value="on" />';
?>
<?php

include("../mpdf/mpdf.php");

$mpdf=new mPDF(); 

// $html must be defined
$mpdf->WriteHTML($html);
$mpdf->Output();

exit;

?>
