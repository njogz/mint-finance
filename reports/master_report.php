<?php
if (!isset($_COOKIE['id'])) {
	header("location:index.php");
}
include "../users/users.php";
include "../clients/clients.php";
include "../jobs/jobs.php";

$user = new users;
$client = new clients;
$job = new jobs;
?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <title>Mintgraphics Finance</title>
    <meta content="IE=edge,chrome=1" http-equiv="X-UA-Compatible">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="Njuguna Ndung'u">

    <link rel="stylesheet" type="text/css" href="../lib/bootstrap/css/bootstrap.css">
    
    <link rel="stylesheet" type="text/css" href="../stylesheets/theme.css">
    <link rel="stylesheet" href="../lib/font-awesome/css/font-awesome.css">
    <link rel="stylesheet" href="http://code.jquery.com/ui/1.10.3/themes/smoothness/jquery-ui.css">

    <script src="../lib/jquery-1.7.2.min.js" type="text/javascript"></script>
    <script src="http://code.jquery.com/jquery-1.9.1.js"></script>
	<script src="http://code.jquery.com/ui/1.10.3/jquery-ui.js"></script>
	<script>
	$(function() {
		$( "#startdate" ).datepicker();
		$( "#enddate" ).datepicker();
	 });
	 </script>

    <!-- Demo page code -->

    <style type="text/css">
        #line-chart {
            height:300px;
            width:800px;
            margin: 0px auto;
            margin-top: 1em;
        }
        .brand { font-family: georgia, serif; }
        .brand .first {
            color: #ccc;
            font-style: italic;
        }
        .brand .second {
            color: #fff;
            font-weight: bold;
        }
    </style>

    <!-- Le HTML5 shim, for IE6-8 support of HTML5 elements -->
    <!--[if lt IE 9]>
      <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->

    <!-- Le fav and touch icons -->
    <link rel="shortcut icon" href="../assets/ico/favicon.ico">
    <link rel="apple-touch-icon-precomposed" sizes="144x144" href="../assets/ico/apple-touch-icon-144-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="114x114" href="../assets/ico/apple-touch-icon-114-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="72x72" href="../assets/ico/apple-touch-icon-72-precomposed.png">
    <link rel="apple-touch-icon-precomposed" href="../assets/ico/apple-touch-icon-57-precomposed.png">
  </head>

  <!--[if lt IE 7 ]> <body class="ie ie6"> <![endif]-->
  <!--[if IE 7 ]> <body class="ie ie7 "> <![endif]-->
  <!--[if IE 8 ]> <body class="ie ie8 "> <![endif]-->
  <!--[if IE 9 ]> <body class="ie ie9 "> <![endif]-->
  <!--[if (gt IE 9)|!(IE)]><!--> 
  <body class=""> 
  <!--<![endif]-->
    
    <div class="navbar">
        <div class="navbar-inner">
                <ul class="nav pull-right">
                    
                    <!-- <li><a href="#" class="hidden-phone visible-tablet visible-desktop" role="button">Settings</a></li> -->
                    <li id="fat-menu" class="dropdown">
                        <a href="#" role="button" class="dropdown-toggle" data-toggle="dropdown">
                            <i class="icon-user"></i>
                            <?php
								$name = $user->get_user("user_name",$_COOKIE['id']);
								echo $name;                            
                            ?>
                            <i class="icon-caret-down"></i>
                        </a>

                        <ul class="dropdown-menu">
							<?php
								define('ROOT', getcwd());
								require_once ROOT . '../../menu/usermenu.php';
							?>
                        </ul>
                    </li>
                    
                </ul>
                <a class="brand" href="index.html"><span class="first">Mint</span> <span class="second">Graphics</span></a>
        </div>
    </div>
    <div class="sidebar-nav">
        <form class="search form-inline">
            <input type="text" placeholder="Search...">
        </form>

        <?php
			define('ROOT', getcwd());
			require_once ROOT . '../../menu/sidemenu.php';
		?>
    </div>
    

    
    <div class="content">
        
        <div class="header">
            
            <h1 class="page-title">Master report</h1>
        </div>
        
                <ul class="breadcrumb">
            <li><a href="../dashboard.php">Home</a> <span class="divider">/</span></li>
            <li class="active">Master report</li>
        </ul>

        <div class="container-fluid">
            <div class="row-fluid">
                    

<div class="row-fluid">
	<?php
		if(isset($_POST['startdate'])){
			$start_date=$_POST['startdate'];
			$end_date=$_POST['enddate'];
			list($start_month,$start_date,$start_year)=explode("/",$start_date);
			$startdate=$start_year."-".$start_month."-".$start_date;
			list($end_month,$end_date,$end_year)=explode("/",$end_date);
			$enddate=$end_year."-".$end_month."-".$end_date;
			if ($startdate>$enddate){
				echo '<div class="alert alert-error">
					<button type="button" class="close" data-dismiss="alert">×</button>
					The start date cannot be more than the end date!
				</div>';
			}
			else{
				echo '<div class="alert alert-info">
					<button type="button" class="close" data-dismiss="alert">×</button>
					Sales between '.$startdate.' and '.$enddate.'
				</div>';
			}
		}
	?>

   <!-- <div class="alert alert-info">
        <button type="button" class="close" data-dismiss="alert">×</button>
        <strong>Just a quick note:</strong> Hope you like the theme!
    </div> -->
    <form method="post" action="sales_report.php">
	Start Date: <input type="text" name="startdate" id="startdate" /> End Date <input type="text" name="enddate" id="enddate" /> <input type="submit" class="btn btn-primary" value="Search" />
	</form>
    <div class="block ">
        	<?php
			$query="SELECT * FROM expenses, payments ORDER BY expenses.date DESC, payments.date DESC";
			$result=mysql_query($query) or die(mysql_error());
			echo "<table class='table table-bordered'>";
			echo "<thead>";
			  echo "<tr>";
				echo "<th width='20%' scope='row' align='center'><b>Date</b></th>";
				echo "<td width='120' align='center'><b>Amount</b></td>";
				echo "<td width='120' align='center'><b>Type</b></td>";
				echo "<td width='90' align='center'><b>Details</b></td>";
			  echo "</tr>";
			echo "</thead>";
			echo "<tbody>";
			
			if(isset($_POST['startdate'])){
				$job->get_sales_list_period($startdate, $enddate);
			}
			else{
				while ($sales=mysql_fetch_object($result)){
				  echo "<tr>";
					echo "<th scope='row' align='center'>".$sales->date."</th>";
					echo "<td align='center'>".$sales->amount."</td>";
					echo "<td align='center'>".$sales->type."</td>";
					echo "<td align='center'>".$sales->details."</td>";
				  echo "</tr>";
				}
			}
				echo "</tbody>";
				echo "</table>";

			?>
			<p><a class="btn btn-primary" href="sales_report_print.php">Print</a></p>
    </div>
</div>

                    
                    <footer>
                        <hr>
                        
                        <p>&copy; <?php echo date('Y')?> Mint Graphics</p>
                    </footer>
                    
            </div>
        </div>
    </div>
    


    <script src="../lib/bootstrap/js/bootstrap.js"></script>
    <script type="text/javascript">
        $("[rel=tooltip]").tooltip();
        $(function() {
            $('.demo-cancel-click').click(function(){return false;});
        });
    </script>
    
  </body>
</html>


